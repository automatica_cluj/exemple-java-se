package priorityqueue;

import java.util.PriorityQueue;
import java.util.Random;

public class Hospital {
public static void main(String[] args) {
	PriorityQueue pq = new PriorityQueue();
	Random r = new Random();
	for(int i=0;i<10;i++){
		String name = "Patient "+i;
		int eLevel = r.nextInt();
		Patient p = new Patient(name,eLevel);
		System.out.println("New patient arrived:"+p);
		pq.offer(p);
	}
	
	System.out.println("\nTreat patients.\n");
	
	for(int i=0;i<10;i++){
		Patient p = (Patient)pq.poll();
		System.out.println("Treat patient: "+p);
	}	
	
}
}

class Patient implements Comparable{
	String name;
	int emergencyLevel;
	
	Patient(String name, int eL){
		this.name = name;emergencyLevel = eL;
	}

	public int compareTo(Object o) {
		Patient p = (Patient)o;
		if(emergencyLevel>p.emergencyLevel)
			return 1;
		else if(emergencyLevel<p.emergencyLevel)
			return -1;
		return 0;
	}
	
	@Override
	public String toString() {
		return "[patient="+name+" emergencyLevel="+emergencyLevel+"]";
	}
	
}
