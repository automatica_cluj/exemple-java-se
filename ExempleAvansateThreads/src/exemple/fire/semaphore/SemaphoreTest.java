package exemple.fire.semaphore;

/*
 * The wait-notify system in standard Java is not the only way that thread 
 * synchronization can be done. A classic approach in other languages is the 
 * semaphore, which is a protected variable allows up to a given N number of 
 * threads to access a resource simultaneously.
 * 
 * Notice how the results for pass 0 and 1 don't appear until after many other 
 * results. You then get more 20s before seeing more "good" results. Each call 
 * to the average pricing service takes time, therefore the interspersed results.
 * 
 * By going through a semaphore, the program effectively controls access to the 
 * limited resource. 
 */

import java.util.concurrent.*;
import java.util.*;   

public class SemaphoreTest {
  private static final int LOOP_COUNT = 100;
  private static final int MAX_AVAILABLE = 2;
  private final static Semaphore semaphore =
    new Semaphore(MAX_AVAILABLE, true);   

  private static class Pricer {
    private static final Random random = new Random();
    public static int getGoodPrice() {
      int price = random.nextInt(100);
      try {
        Thread.sleep(50);
      } catch (InterruptedException ex) {
        ex.printStackTrace();
      }
      return price;
    }
    public static int getBadPrice() {
      return 20;
    }
  }

  public static void main(String args[]) {
    for (int i=0; i<LOOP_COUNT; i++) {
      final int count = i;
      new Thread() {
        public void run() {
          int price;
         /* if (semaphore.tryAcquire()) {
            try {
            	
              price = Pricer.getGoodPrice();
            } finally {
              semaphore.release();
            }
          } else {
            price = Pricer.getBadPrice();
          }
          */
          try {
			semaphore.acquire();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
          price = Pricer.getGoodPrice();
          semaphore.release();
          System.out.println(count + ": " + price);
        }
      }.start();
    }
  }
}