package lab.scd.xml.sax;
import java.io.*;
import java.util.ArrayList;

import javax.xml.parsers.*;
import org.xml.sax.*;
import org.xml.sax.helpers.*;

public class SAXProcesorXML{
    public static void main(String[] args) {

        // Contruieste un obiect de tip handler care va capta evenimentele generate in momentul parcurgerii documentului 
        DefaultHandler handler = new MyHandler();

        // procesare fisier xml utlizand handlerul construit anterios
        parseXmlFile("persoana.xml", handler, false);
    }

    
    // Aceasta clasa trebuie sa suprascrie metodele din DefaultHandler 
    static class MyHandler extends DefaultHandler {
         
        public void endElement(String uri, String localName, String qName)
                throws SAXException {
            
            System.err.println("Sfarsit element:"+qName);
        }
        
        /**
         * In momentul inceperi parcurgerii documentului este generat evenimentul start document
         * care va fi captat de aceasta metoda. In cadrul ei se initializeaza un vector
         */
        public void startDocument() throws SAXException {
            System.err.println("*Start procesare document");
        }
        
        public void startElement(String uri, String localName, String qName,
                Attributes attributes) throws SAXException {
           
            System.err.println("Start element:"+qName);       
        }
        
        public void characters(char[] ch, int start, int length)
                throws SAXException {
          System.err.println("Continut:"+new String(ch,start,length));       
        }
}

    // Parses an XML file using a SAX parser.
    // If validating is true, the contents is validated against the DTD
    // specified in the file.
    public static void parseXmlFile(String filename, DefaultHandler handler, boolean validating) {
        try {
            
            SAXParserFactory factory = SAXParserFactory.newInstance();
            factory.setValidating(validating);

            // contruieste procesorul si incepe validarea fisir
            // fisiserul este parcurs si va fi generate evenimente in momentul parcurgerii acestui fisier xml
            // evenimentele vor fi captate de catre MyHandler
            factory.newSAXParser().parse(new File(filename), handler);
        } catch (SAXException e) {
           e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

