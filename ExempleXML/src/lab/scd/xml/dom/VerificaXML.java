package lab.scd.xml.dom;
import javax.xml.parsers.*;

/**
 * Clasa verifica daca un document xml este corect formatat (respecta 
 * specificatiile xml).
 */
public class VerificaXML {

    public static void main(String[] args)
    {
        if(args.length != 1)
        {
            System.err.println("Usage: java VerificaXML <xmlFileNameOrUrl>\nExamples:");
            System.err.println("\n\tjava VerificaXML c:\\1.xml");
            
            return;
        }

        try
        {
            DocumentBuilderFactory domFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder domBuilder = domFactory.newDocumentBuilder();
            domBuilder.parse(args[0]);

            System.out.println("'" + args[0] + "' este in format corect.");
        }
        catch(org.xml.sax.SAXException exp)
        {
            System.out.println("'" + args[0] + "' nu este in format corect.\n" + exp.toString());
        }
        catch(FactoryConfigurationError exp)
        {
            System.err.println(exp.toString());
        }
        catch(ParserConfigurationException exp)
        {
            System.err.println(exp.toString());
        }
        catch(Exception exp)
        {
            System.err.println(exp.toString());
        }
    }
}