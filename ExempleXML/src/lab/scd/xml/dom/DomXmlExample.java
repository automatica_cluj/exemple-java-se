/*
 * DomXmlExample.java
 */
package lab.scd.xml.dom;

/**
 * Clasa exemplifica folosirea DOM pentru a construi documente in format XML.
 */
import java.io.*;

import org.w3c.dom.*;

import javax.xml.parsers.*;

import javax.xml.transform.*;
import javax.xml.transform.dom.*;
import javax.xml.transform.stream.*;

public class DomXmlExample {

    /**
     * Our goal is to create a DOM XML tree and then print the XML.
     */
    public static void main (String args[]) {
        new DomXmlExample();
    }

    public DomXmlExample() {
        try {
            /////////////////////////////
            //Construieste un document XML gol

           
            DocumentBuilderFactory dbfac = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = dbfac.newDocumentBuilder();
            Document doc = docBuilder.newDocument();

            ////////////////////////
            //Construieste arborele XML

            //constr. elementul root
            Element root = doc.createElement("root");
            doc.appendChild(root);

            //adauga comentariu in cadrul elementului root
            Comment comment = doc.createComment("Un comentariu");
            root.appendChild(comment);

            //adauga un element copil in root
            Element child = doc.createElement("child");
            child.setAttribute("name", "value");
            root.appendChild(child);

            //add a text element to the child
            Text text = doc.createTextNode("Un sir de caractere adaugat in cadrul unui element!");
            child.appendChild(text);

            /////////////////
            //Generare string XML

            //construieste un obiect transformer
            TransformerFactory transfac = TransformerFactory.newInstance();
            Transformer trans = transfac.newTransformer();
            trans.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
            trans.setOutputProperty(OutputKeys.INDENT, "yes");

            //construieste string pe baza arborelui XML
            StringWriter sw = new StringWriter();
            StreamResult result = new StreamResult(sw);
            DOMSource source = new DOMSource(doc);
            trans.transform(source, result);
            String xmlString = sw.toString();

            //print xml
            System.out.println("Sir xml generat:\n\n" + xmlString);

        } catch (Exception e) {
            System.out.println(e);
        }
    }
}