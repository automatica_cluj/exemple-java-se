/*
 * Citeste un fisier XML folosind DOM
 * TestDOM.java
 */
package lab.scd.xml.dom;

import javax.xml.parsers.*;
import org.w3c.dom.*;

public class TestDOM
{
   public static void main( String [] args ) throws Exception
   {
      DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
      DocumentBuilder parser = factory.newDocumentBuilder();
      Document document = parser.parse( "persoana.xml" );
      Element inventory = document.getDocumentElement();
      NodeList lst = inventory.getElementsByTagName("persoana");

      System.out.println("Persoane:"+lst.getLength());
      for( int i=0; i<lst.getLength(); i++ ) {
         String n = DOMUtil.getSimpleElementText( 
            (Element)lst.item(i),"nume" );
         String p = DOMUtil.getSimpleElementText( 
            (Element)lst.item(i), "prenume" );
         System.out.println( "  "+ n +" ("+p+")" );
      }
   
   }
}

class DOMUtil
{
   public static Element getFirstElement( Element element, String name ) {
      NodeList nl = element.getElementsByTagName( name );
      if ( nl.getLength() < 1 )
         throw new RuntimeException(
            "Element: "+element+" does not contain: "+name);
      return (Element)nl.item(0);
   }

   public static String getSimpleElementText( Element node, String name ) 
   {
      Element namedElement = getFirstElement( node, name );
      return getSimpleElementText( namedElement );
   }

   public static String getSimpleElementText( Element node ) 
   {
      StringBuffer sb = new StringBuffer();
      NodeList children = node.getChildNodes();
      for(int i=0; i<children.getLength(); i++) {
         Node child = children.item(i);
         if ( child instanceof Text )
            sb.append( child.getNodeValue() );
      }
      return sb.toString();
   }
}