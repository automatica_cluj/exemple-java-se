package colectii.map;

import java.util.*;
import java.io.*;

public class Dictionar2 {
	
	   HashMap dct = new HashMap();

	   public void adaugaCuvant(Cuvant c, String definitie) {
	      
		  if(dct.containsKey(c)){
			  System.out.println("Modific cuvant existent!");
		  }
		  else
		  {
			 System.out.println("Adauga cuvant nou.");
		  }  
		  dct.put(c, definitie);
	      
	   }

	   public String cautaCuvant(Cuvant c) {
	      return (String)dct.get(c);
	   }

	   public void afisDictionar() {
	      System.out.println(dct);
	   }


	   public static void main(String args[]) throws Exception {
	      Dictionar2 dict = new Dictionar2();
	      char raspuns;
	      String linie, explic;
	      BufferedReader fluxIn = new BufferedReader(new InputStreamReader(System.in));

	      do {
	         System.out.println("Meniu");
	         System.out.println("a - Adauga cuvant");
	         System.out.println("c - Cauta cuvant");
	         System.out.println("l - Listeaza dictionar");
	         System.out.println("e - Iesi");

	         linie = fluxIn.readLine();
	         raspuns = linie.charAt(0);

	         switch(raspuns) {
	            case 'a': case 'A':
	               System.out.println("Introduceti cuvantul:");
	               linie = fluxIn.readLine();
	               if( linie.length()>1) {
	                  System.out.println("Introduceti definitia:");
	                  explic = fluxIn.readLine();
	                  dict.adaugaCuvant(new Cuvant(linie), explic);
	               }
	            break;
	            case 'c': case 'C':
	               System.out.println("Cuvant cautat:");
	               linie = fluxIn.readLine();
	               Cuvant x = new Cuvant(linie);
	               if( linie.length()>1) {
	                  explic = dict.cautaCuvant(x);
	                  if (explic == null)
	                     System.out.println("nu exista");
	                  else
	                     System.out.println("Explicatie:"+explic);
	               }
	            break;
	            case 'l': case 'L':
	               System.out.println("Afiseaza:");
	               dict.afisDictionar();
	            break;
	            
	         }
	      } while(raspuns!='e' && raspuns!='E');
	      System.out.println("Program terminat.");
	   }
	}

class Cuvant{
	String c;
	public Cuvant(String c) {
		this.c = c;
	}

	public boolean equals(Object obj) {	
		return c.equals((String)obj);
	}
	
	public int hashCode() {
		return (int)(Math.random()*c.length()*1000);	
	}
	
	public String toString() {
		return c;
	}	
}