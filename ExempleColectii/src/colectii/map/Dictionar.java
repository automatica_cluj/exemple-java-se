package colectii.map;
import java.util.*;
import java.io.*;

public class Dictionar {
	
	   Map<String,String> dct = new HashMap<>();

	   public Dictionar() {
	   }

	   public void adaugaCuvant(String cuvant, String definitie) {
	      
		  if(dct.containsKey(cuvant)){
			  System.out.println("Modific cuvant existent!");
		  }
		  else
		  {
			 System.out.println("Adauga cuvant nou.");
		  }  
		  dct.put(cuvant, definitie);
	      
	   }

	   public String cautaCuvant(String cuvant) {
	      return dct.get(cuvant);
	   }

	   public void afisDictionar() {
	      System.out.println(dct);
	   }


	   public static void main(String args[]) throws Exception {
	      Dictionar dict = new Dictionar();
	      char raspuns;
	      String linie, explic;
	      BufferedReader fluxIn = new BufferedReader(new InputStreamReader(System.in));

	      do {
	         System.out.println("Meniu");
	         System.out.println("a - Adauga cuvant");
	         System.out.println("c - Cauta cuvant");
	         System.out.println("l - Listeaza dictionar");
	         System.out.println("e - Iesi");

	         linie = fluxIn.readLine();
	         raspuns = linie.charAt(0);

	         switch(raspuns) {
	            case 'a': case 'A':
	               System.out.println("Introduceti cuvantul:");
	               linie = fluxIn.readLine();
	               if( linie.length()>1) {
	                  System.out.println("Introduceti definitia:");
	                  explic = fluxIn.readLine();
	                  dict.adaugaCuvant(linie, explic);
	               }
	            break;
	            case 'c': case 'C':
	               System.out.println("Cuvant cautat:");
	               linie = fluxIn.readLine();
	               if( linie.length()>1) {
	                  explic = dict.cautaCuvant(linie);
	                  if (explic == null)
	                     System.out.println("nu exista");
	                  else
	                     System.out.println("Explicatie:"+explic);
	               }
	            break;
	            case 'l': case 'L':
	               System.out.println("Afiseaza:");
	               dict.afisDictionar();
	            break;
	            
	         }
	      } while(raspuns!='e' && raspuns!='E');
	      System.out.println("Program terminat.");
	   }
	}
