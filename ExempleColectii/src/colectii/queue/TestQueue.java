package colectii.queue;

import java.util.PriorityQueue;

public class TestQueue {
public static void main(String[] args) {
	Job j1 = new Job("chek trains on input rail segments",3);
	Job j2 = new Job("chek trains on ouput rail segments",2);
	Job j3 = new Job("chek trains on rail station segments",1);
	
	PriorityQueue<Job> que = new PriorityQueue<Job>();
	que.offer(j1);
	que.offer(j2);
	que.offer(j3);
	
	while(que.size()!=0){
		Job j = (Job)que.poll();
		j.execute();
	}	
}
}

class Job implements Comparable{
	
	int priority;
	String name;

	public Job(String name,int priority) {
		this.priority = priority;
		this.name = name;
	}
	
	public void execute(){
		System.out.println("Execute job:"+name+" - job priority="+priority);
	}

	public int compareTo(Object o) {	
		Job x = (Job)o;
		if(priority>x.priority){
			return 1;
		}else if(priority==x.priority)
			return 0;
		else
			return -1;
	}	
}
