/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package colectii.comparare;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 *
 * @author evo
 */
public class TestSortare {
    public static void main(String[] args) {
        
     
        
        ArrayList<Product> list = new ArrayList<>();
        list.add(new Product("A", 7));
        list.add(new Product("B", 5));
        list.add(new Product("C", 4));
        
        Collections.sort(list);
        

        for(Product p: list)
            System.out.println(p);
        
        Comparator<Product> NAME_ORDER = 
                                        new Comparator<Product>() {
            public int compare(Product e1, Product e2) {
                return e1.getName().compareTo(e2.getName());
            }
         };
        
        Collections.sort(list, NAME_ORDER);
        list.sort(NAME_ORDER);
        for(Product p: list)
            System.out.println(p);
        
        
    }
}

  

class Product implements Comparable<Product>{
    private String name;
    private int price;

    public Product(String name, int price) {
        this.name = name;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public int getPrice() {
        return price;
    }

    @Override
    public int compareTo(Product o) {
        return this.price - o.price;
    }
    
    public String toString(){
        return "[name="+name+" price="+price+"]";
    }
    
}
