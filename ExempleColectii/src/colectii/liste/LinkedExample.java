package colectii.liste;
import java.util.*;
/**
 * Exemplificare utilizare colectie LinkedList.
 */
public class LinkedExample {
	public static void main(String[] args) {
		LinkedList<Command> lk = new LinkedList<>();
		lk.addFirst(new Command("comanda 1"));
		lk.addFirst(new Command("comanda 2"));
		lk.addFirst(new Command("comanda 3"));
		
		Command c = lk.removeLast();
		c.execute();
		c = lk.removeLast();
		c.execute();
		c = lk.removeLast();
		c.execute();		
	}
}

class Command{
	String name;
	Command(String n){name = n;}
	void execute(){System.out.println("Execute command:"+name);}
}
