/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package colectii.liste;

import java.util.ArrayList;

/**
 *
 * @author evo
 */
public class IteratorTestCurs {
    public static void main(String[] args){
        NameContainer n1 = new NameContainer();
        Iterator i = n1.iterator();
        while(i.hasNext()){
            String s = (String)i.next();
            System.out.println(s);
        }
    }
}

interface Iterator{
    boolean hasNext();
    Object next();
}

interface Container{
    Iterator iterator();
}

class NameContainer implements Container{

    //private String[]  list = new String[]{"asd","sdsd","ssdd"};
    ArrayList<String> list = new ArrayList<>();

    public NameContainer() {
    list.add("asd");
    list.add("lkjasd");
    }
    
    
    
    @Override
    public Iterator iterator() {
        return new NameIterator();
    }
    
    private class NameIterator implements Iterator{

        int index;
        
        @Override
        public boolean hasNext() {
            return index < list.size();
        }

        @Override
        public Object next() {
            if(hasNext())
                return list.get(index++);
           return null;
        }        
    }
    
}