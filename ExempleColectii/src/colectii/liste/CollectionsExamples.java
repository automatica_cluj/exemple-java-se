
package colectii.liste;

import java.util.*;


/**
 * Exercitiu:
 * Se se realizeze o colectie care stocheaza o lista de obiecte de tip Senzor.
 * Senzor are atributele id de tip intreg si locatie de tip string. 
 * Sa se adauge 5 obiecte in lista. 
 * Sa se stearga dein lista obiectul de pe pozita 2.
 * Sa se caute in lista un obiecte si sa se stearga daca acesta exista. 
 * 
 */
public class CollectionsExamples {

    
    public static void main(String[] args) {
        ArrayList<Order> a = new ArrayList<>();
        
        a.add(new Order(1,"1asdas"));
        a.add(new Order(2,"2asdas"));
        a.add(new Order(3,"3asacas"));
        a.add(new Order(4,"4dwefwrv"));
        
        for(int i=0;i<a.size();i++){
            Order x = a.get(i);
            System.out.println("Order:"+x.client+":"+x.id);
        }
        
        a.remove(3);
        
        System.out.println("-----------");
        
        for(int i=0;i<a.size();i++){
            Order x = a.get(i);
            System.out.println("Order:"+x.client+":"+x.id);
        }
        
        Order o1 = new Order(3, "3uysudygs");
        
        if(a.contains(o1)){
             System.out.println("Obiect gasit!");
        }
        else{
             System.out.println("Obiect inexistent.");
        }
        
        a.remove(o1);
        
         System.out.println("-----------");
        
        for(int i=0;i<a.size();i++){
            Order x = a.get(i);
            System.out.println("Order:"+x.client+":"+x.id);
        }
        //System.out.println(a);
        //Collections.shuffle(a);
        //System.out.println(a);
        
    
    }
    
   
}

class Order {
    int id;
    String client;

    public Order(int id, String client) {
        this.id = id;
        this.client = client;
    }
    
    void displayOrder(){
        System.out.println("Order id "+client +" client "+client);
    }
    
    public boolean equals(Object o){
        Order t = (Order)o;
        return t.id==id;
    }
    
    public String toString(){
        return "["+id+" "+client+"]";
    }
    
}