/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package colectii.liste;

import java.util.ArrayList;

/**
 *
 * @author evo
 */
public class GenericExample {
    
    static void test1(){
        ArrayList l1 = new ArrayList();
        l1.add(new BankAccount("123"));
        l1.add(new BankAccount("342"));
        l1.add(new Order("Client1","342"));
        
        Object o = l1.get(0);
        BankAccount o1 = (BankAccount)l1.get(0);
        Order o2 = (Order)l1.get(0);
        o1.displayAccount();
        o2.displayOrder();
    }
    
    static void test2(){
        ArrayList l1 = new ArrayList();
        l1.add(new BankAccount("123"));
        l1.add(new BankAccount("342"));
        l1.add(new Order("Client1","342"));
        
        Object o = l1.get(0);
        BankAccount o1 = (BankAccount)l1.get(0);
        Order o2 = (Order)l1.get(0);
        o1.displayAccount();
        o2.displayOrder();
    }
    
    
    public static void main(String[] args) {
        ArrayList<BankAccount> l1 = new ArrayList<>();
        l1.add(new BankAccount("123"));
        l1.add(new BankAccount("342"));
        l1.add(new Order("Client1","342"));
        

        BankAccount o1 = l1.get(0);
        Order o2 = (Order)l1.get(0);
        o1.displayAccount();
        o2.displayOrder();        
    }


class BankAccount{
    String iban;

    public BankAccount(String iban) {
        this.iban = iban;
    }
    
    void displayAccount(){
        System.out.println("Account "+iban);
    }
    
}

class Order {
    String id;
    String client;

    public Order(String id, String client) {
        this.id = id;
        this.client = client;
    }
    
    void displayOrder(){
        System.out.println("Order id "+client +" client "+client);
    }
    
}

}