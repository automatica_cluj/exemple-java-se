package colectii.liste;

import java.util.*;
/**
 * Exemplu ArrayList
 */
public class ArrayListExample {

public static void main(String[] args) {
	List<String> z1 = new ArrayList<>();
	ArrayList<String> z2 = new ArrayList<>();	
	z1.add("dog");
	z1.add("cat");	
	z2.add("bird");	
	z1.set(0, "elephant");	
	z2.addAll(z1);
	//parcurgere folosind for
	System.out.println("Zoo 2:");
	for(int i=0;i<z2.size();i++){
		String animal = z2.get(i);
		System.out.println(animal);
	}
	//parcurgere folosind structura foreach
	System.out.println("Zoo 1:");
	for(Object o:z1){
		System.out.println(o);
	}
	//parcurgere folosind iteratori
	Iterator<String> it = z1.iterator();
	while(it.hasNext()){
		String s = it.next();
		System.out.println(s);
	}
	//sau
	for(Iterator ix=z2.iterator();ix.hasNext();){
		String s = (String)it.next();
		System.out.println(s);
	}
		
}

}
