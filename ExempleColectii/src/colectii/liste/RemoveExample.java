package colectii.liste;
import java.util.*;
public class RemoveExample {
	static void displayAll(List l){
		System.out.println("Display all persons.");
		for (Object p : l) {
			System.out.println(p);
		}
	}
	
	public static void main(String[] args) {
		List c = new ArrayList();
		Person p1 = new Person("aaa","bbb");
		Person p2 = new Person("ccc","ddd");
		Person p3 = new Person("xxx","yyy");
		Person p4 = new Person("zzz","qqq");
		
		c.add(p1);c.add(p2);c.add(p3);c.add(p4);
		displayAll(c);
		c.remove(p2);
		displayAll(c);
		
		Person p5 = new Person("aaa","bbb");
		c.remove(p5);
		displayAll(c);
		
		c.remove(0);
		displayAll(c);
	}
}

class Person{
	String firstname;
	String lastname;
	Person(String f, String l){
		this.firstname = f;this.lastname = l;
	}
	
	public boolean equals(Object obj) {
		if(obj instanceof Person){
			Person p = (Person)obj;
			return p.firstname.equalsIgnoreCase(firstname)&&p.lastname.equalsIgnoreCase(lastname);		
		}
		else return false;
	}
	
	public String toString() {
		return "persoana:"+firstname+":"+lastname;
	}
}