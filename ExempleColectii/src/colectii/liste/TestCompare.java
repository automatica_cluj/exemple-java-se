package colectii.liste;

import java.util.*;

public class TestCompare {

    public static void main(String[] args) {
        Product p1 = new Product("A", 1);

        ArrayList<Product> list = new ArrayList<>();
        list.add(p1);
        list.add(new Product("C", 2));
        list.add(new Product("B", 9));

        for (int i = 0; i < list.size(); i++) {
            Product x = list.get(i);
            System.out.println(x.getPrice());            
        }
        
        for (Product p : list) {
            System.out.println(p.getPrice());         
        }
        
        TreeSet<Product> set = new TreeSet<>();

        Iterator<Product> i = set.iterator();
        while(i.hasNext()){
            Product p = i.next();
             System.out.println(p.getPrice()); 
        }
        
        
     /*   System.out.println(list);

        System.out.println("Sort by name.");
        Collections.sort(list);
        System.out.println(list);

        System.out.println("Sort by price.");
        //Collections.sort(list, new PriceSort());

        Collections.sort(list, new Comparator() {

            @Override
            public int compare(Object o1, Object o2) {
                Product p1 = (Product) o1;
                Product p2 = (Product) o2;
                if (p1.getPrice() < p2.getPrice()) {
                    return -1;
                } else if (p1.getPrice() == p2.getPrice()) {
                    return 0;
                } else {
                    return 1;
                }
            }

        });
        System.out.println(list);
*/
    }
}

/*class PriceSort implements Comparator{

 @Override
 public int compare(Object o1, Object o2) {
 Product p1 = (Product)o1;
 Product p2 = (Product)o2;
 if(p1.getPrice()<p2.getPrice())
 return -1;
 else if(p1.getPrice()==p2.getPrice())
 return 0;
 else
 return 1;
 }
    
 }*/
class Product implements Comparable {

    private String name;
    private int price;

    public Product(String name, int price) {
        this.name = name;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public int getPrice() {
        return price;
    }

    @Override
    public String toString() {
        return "Product{" + "name=" + name + ", price=" + price + '}';
    }

    @Override
    public int compareTo(Object o) {

        Product p = (Product) o;
        return name.compareTo(p.getName());
        /*if(p.getPrice()<price)
         return -1;
         else if(p.getPrice()==price)
         return 0;
         else
         return 1;
         */
    }

}
