package exemple.fire.consumerproducer;

import java.util.Random;
import java.util.concurrent.*;

class Producer implements Runnable {
	   private Random r = new Random();
	   private final BlockingQueue queue;
	   
	   Producer(BlockingQueue q) { queue = q; }
	   
	   public void run() {
	     try {
	       while(true) {
	    	   Component com = produce();
	    	   System.out.println("Add component:"+com);
	    	   queue.put(com); 
	    	   }
	     } catch (InterruptedException ex) {ex.printStackTrace();}
	   }
	   
	   Component produce() { 
		   int y = r.nextInt();
		   Component c = new Component(y);
		   return c;
	   }
	 }

	 class Consumer implements Runnable {
	   private final BlockingQueue queue;
	   Consumer(BlockingQueue q) { queue = q; }
	   public void run() {
	     try {
	       while(true) { consume(queue.take()); }
	     } catch (InterruptedException ex) {ex.printStackTrace();}
	   }
	   void consume(Object x) {
		   Component c = (Component)x;
		   c.process();
		   System.out.println("Object "+x+" processed.");
		   
		   }
	 }

	 public class Main {
	   public static void main(String[] args) {
	     BlockingQueue q = new ArrayBlockingQueue(4);
	     Producer p = new Producer(q);
	     Consumer c1 = new Consumer(q);
	     Consumer c2 = new Consumer(q);
	     new Thread(p).start();
	     new Thread(c1).start();
	     new Thread(c2).start();
	   }
	 }
	 
	 
	 class Component{
		int type;
		boolean processed = false;
		
		Component(int type){this.type = type;}
		
		void process(){
			//simulate some processind 
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			processed  =true;
		}
		
		@Override
		public String toString() {
			return "[type="+type+" processed="+processed+"]";
		}
	 }