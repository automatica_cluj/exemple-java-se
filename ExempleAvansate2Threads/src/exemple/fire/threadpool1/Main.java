package exemple.fire.threadpool1;

import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * A ThreadPoolExecutor that can additionally schedule commands to run after a given delay, or to execute periodically. 
 * This class is preferable to Timer  when multiple worker threads are needed, or when the additional flexibility or capabilities 
 * of ThreadPoolExecutor (which this class extends) are required. 
 * 
 * Delayed tasks execute no sooner than they are enabled, but without any real-time guarantees about when, after they are enabled, 
 * they will commence. Tasks scheduled for exactly the same execution time are enabled in first-in-first-out (FIFO) order of 
 * submission. 
 * 
 * @author mihai
 *
 */

public class Main{

  public static void main(String args[]) {
    ScheduledThreadPoolExecutor stpe = new ScheduledThreadPoolExecutor(5);
    
    stpe.scheduleAtFixedRate(new Job1(), 0, 5, TimeUnit.SECONDS);
    stpe.scheduleAtFixedRate(new Job2(), 1, 2, TimeUnit.SECONDS);
  }
}

class Job1 implements Runnable {
  public void run() {
    System.out.println("Job 1");
  }
}

class Job2 implements Runnable {
  public void run() {
      for(int i=-3;i<3;i++){
        System.out.println(i);
      }
  }
}