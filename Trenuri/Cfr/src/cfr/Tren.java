
package cfr;

public class Tren {
    private int id;
    private String statia_plecare;
    private String statia_sosire;
    private int ora_plecare;
    private int stare;

    public Tren(int id, String statia_plecare, String statia_sosire, int ora_plecare, int stare) {
        this.id = id;
        this.statia_plecare = statia_plecare;
        this.statia_sosire = statia_sosire;
        this.stare = stare;
        this.ora_plecare = ora_plecare;
    }
    
    int getOraPlecare(){
        return ora_plecare;
    }
    
    void setStare(int s){
        stare = s;
    }
    
    int getStare(){
        return stare;
    }
    
    void afisare(){
        System.out.println("Tren "+id+" "+statia_plecare+" "+statia_sosire+" stare="+stare+" ora plecare="+ora_plecare);
    }
}
