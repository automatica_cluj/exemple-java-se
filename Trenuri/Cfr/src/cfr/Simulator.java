
package cfr;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @author Mihai Hulea mihai.hulea@aut.utcluj.ro
 */
public class Simulator{
    
    int timp = 0;
    ConexiuneDb db;
    String gara;

    public Simulator(String gara, ConexiuneDb db) {
        this.db = db;
        this.gara = gara;
    }
    
    void simuleaza(){
        while(true){
            timp++;
            System.out.println("Timpul curent este:"+timp);
            ArrayList<Tren> lista =
                    db.cautaTrenuri(gara, timp);
            if(lista.size()>0){
                for(Tren t:lista){
                    System.out.println("PLEACA TRENUL");
                    t.afisare();
                }
            }else{
                System.out.println("Nu exista tren!");
            }
            try {
                Thread.sleep(1000);
            } catch (InterruptedException ex) {
                Logger.getLogger(Simulator.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
    

