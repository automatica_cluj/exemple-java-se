
package cfr;
import java.sql.*;
import java.util.*;

public class Cfr {

  
    public static void main(String[] args) throws Exception {
        System.out.println("Conectare...");
	Class.forName("com.mysql.jdbc.Driver");
			
	Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/cfr?user=root");
			
        System.out.println("Conexiune realizata.");
        
        //////////////////
        System.out.println("Lista trenuri");
        ConexiuneDb dbcfr = new ConexiuneDb();
        ArrayList<Tren> lista = dbcfr.cautaTrenuri("bucuresti");
        for(Tren t:lista){
            t.afisare();
        }
        
         //////////////////
        
        System.out.println("Lista trenuri cu ora de plecare data");
        lista = dbcfr.cautaTrenuri("bucuresti",12);
        for(Tren t:lista){
            t.afisare();
        }
        
         //////////////////
        
        Simulator simulatorBucuresti = new Simulator("Bucuresti", dbcfr);
        simulatorBucuresti.simuleaza();
        
    }
    
}
