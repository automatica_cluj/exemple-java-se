
package cfr;

import java.sql.*;
import java.util.ArrayList;

public class ConexiuneDb {
    Connection con;

    public ConexiuneDb() throws Exception{
        Class.forName("com.mysql.jdbc.Driver");			
	con= DriverManager.getConnection("jdbc:mysql://localhost/cfr?user=root");
    }  
    
    ArrayList<Tren> cautaTrenuri(String statia_plecare){
        ArrayList<Tren> lista = new ArrayList<Tren>();
        try{
        Statement st = con.createStatement();
        ResultSet result =
                st.executeQuery("SELECT * FROM merstrenuri WHERE statie_plecare='"+statia_plecare+"';");
        while(result.next()){
            int id = result.getInt(1);
            String sp = result.getString(2);
            String ss = result.getString(3);
            int ora = result.getInt(4);
            int stare = result.getInt(5);
            Tren t = new Tren(id, sp, ss, ora, stare);
            lista.add(t);            
        }
        
        }catch(Exception e){
            e.printStackTrace();
        }
        return lista;
                
    }
    
    ArrayList<Tren> cautaTrenuri(String statia_plecare, int ora_plecare){
        ArrayList<Tren> lista = new ArrayList<Tren>();
        try{
        Statement st = con.createStatement();
        ResultSet result =
                st.executeQuery("SELECT * FROM merstrenuri WHERE statie_plecare='"+statia_plecare+"' AND ora_plecare="+ora_plecare+";");
        while(result.next()){
            int id = result.getInt(1);
            String sp = result.getString(2);
            String ss = result.getString(3);
            int ora = result.getInt(4);
            int stare = result.getInt(5);
            Tren t = new Tren(id, sp, ss, ora, stare);
            lista.add(t);            
        }
        
        }catch(Exception e){
            e.printStackTrace();
        }
        return lista;
                
    }
    
}
