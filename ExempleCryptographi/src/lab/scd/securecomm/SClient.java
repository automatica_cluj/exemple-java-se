package lab.scd.securecomm;
import java.net.*;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Security;
import java.util.Arrays;
import  java.io.*;

import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
public class SClient {
	
	static KeyPairGenerator rsaKeyPairGen;
	static KeyPair rsaKeyPair;
	static PublicKey rsaPub;
	static PrivateKey rsaPriv;
	static Cipher cp;
	
	public static String initEncrypt(){
//		 Incarca BoncyCastle security provider
		try {
		    Security.addProvider(new BouncyCastleProvider());
		}
		catch(Exception e) {
			System.err.println("Eraoare incarcare security provider (" +e.getMessage() + ")");
		}
		
		// Genereaza perechea cheie publica\privata
		try {
		    rsaKeyPairGen = KeyPairGenerator.getInstance("RSA");
		} catch (NoSuchAlgorithmException ex) {
		    System.out.println("Eroare initializare generator chei.");
		    return "cryp error";
		} 
		
		// initializeaza generatorul de chei
		rsaKeyPairGen.initialize(1024);
		
		//genereaza perechea de chei
		rsaKeyPair = rsaKeyPairGen.generateKeyPair();
		if (rsaKeyPair == null) {
		    System.out.println("Eroare generare pereche cheie publica - privata");
		    return "cryp error";
		}
		
		// cheia publica RSA
		rsaPub = rsaKeyPair.getPublic();
		
		// cheia privata RSA
		rsaPriv = rsaKeyPair.getPrivate();
		
		return "";
	}
	
	public static String encrypt(String text){
		
		
		byte[] cipherText = null, newPlainText = null;
		
		
		
		// initializeaza obiectul de tip Cipher folosit pentru criptare decriptare 
		try {
		    cp = Cipher.getInstance("RSA");
		} catch (NoSuchAlgorithmException ex) {
		    System.out.println("Algoritmul de criptare RSA nu exista.");
		    return "cryp error";
		} catch (NoSuchPaddingException ex) {
		
		    System.out.println("Eroare initializare Cipher.");
		    return "cryp error";
		}
		
		// Criptare
		try {
		    cp.init(Cipher.ENCRYPT_MODE, rsaPub);	    
		    cipherText = cp.doFinal(text.getBytes());
		   // System.out.println("Text criptat:"+new String(cipherText));
		    return new String(cipherText);
		} catch (Exception ex) {
		System.out.println("Eroare criptare: " + ex.getMessage());
		return "cryp error";
		}
	
	}
	
	static BufferedReader line = new BufferedReader(new InputStreamReader(System.in));
	public static String readKeyboard() throws Exception{		
		return line.readLine();
	}
	
	public static void main(String[] args) throws Exception{
		
			initEncrypt();
		  Socket socket=null;
		    try {
		      //creare obiect address care identifica adresa serverului
		      InetAddress server_address =InetAddress.getByName("localhost");
		      //se putea utiliza varianta alternativa: InetAddress.getByName("127.0.0.1")
		      
		      socket = new Socket(server_address,1900);

		      //construieste fluxul de intrare prin care sunt receptionate datele de la server
		      BufferedReader in =
		        new BufferedReader(
		          new InputStreamReader(
		            socket.getInputStream()));
		      
		      //construieste fluxul de iesire prin care datele sunt trimise catre server
		      // Output is automatically flushed
		      // by PrintWriter:
		      PrintWriter out =
		        new PrintWriter(
		          new BufferedWriter(
		            new OutputStreamWriter(
		              socket.getOutputStream())),true);

		      
		      for(int i = 0; i < 10; i ++) {
		        String txt = SClient.readKeyboard();
		        txt = SClient.encrypt(txt);
		        out.print(txt);
		        out.flush();
		      }
		      
		      out.println("END"); //trimite mesaj care determina serverul sa inchida conexiunea

		    }
		    catch (Exception ex) {ex.printStackTrace();}
		    finally{
		      socket.close();
		    }

		
	}
	
}
