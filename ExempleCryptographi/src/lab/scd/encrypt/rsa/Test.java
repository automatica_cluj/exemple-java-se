/*
 * Test.java
 */
package lab.scd.encrypt.rsa;
import java.security.*;
import java.util.*;

import org.bouncycastle.jce.provider.BouncyCastleProvider;

/**
 * Class created by @author HULEA Mihai.
 * 
 * This class is part of the laborator4_2crypto project.
 * Afiseaza lista JCE providers instalati.
 */
public class Test {

 public static void main(String[] args){
	 
//	 Incarca BoncyCastle security provider
		try {
		    Security.addProvider(new BouncyCastleProvider());
		}
		catch(Exception e) {
			System.err.println("Eraoare incarcare security provider (" +e.getMessage() + ")");
		}
		
  Provider[] providers = Security.getProviders();
		for (int i = 0; i < providers.length; i++) {
			Provider provider = providers[i];
			System.out.println("Provider name: " + provider.getName());
			System.out.println("Provider information: " + provider.getInfo());
			System.out.println("Provider version: " + provider.getVersion());
			System.out.println("- - - ");
			Set entries = provider.entrySet();
			Iterator iterator = entries.iterator();
			while (iterator.hasNext()) {
				System.out.println("Property entry: " + iterator.next());
			}
		} 
 	}
}
