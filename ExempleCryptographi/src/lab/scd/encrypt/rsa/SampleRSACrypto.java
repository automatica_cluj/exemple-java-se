/*
 * SampleRSACrypto.java
 */
package lab.scd.encrypt.rsa;

/**
 * Class created by @author Mihai HULEA.
 * 
 * This class is part of the laborator4_2crypto project.
 * 
 * Aplicatia exemplifica modul de reliazare a criptarii\decriptarii unui text 
 * folosind algoritmul RSA cu cheie asimetrica. 
 */
import java.io.*;
import java.util.Arrays;
import java.security.Security;
import java.security.Provider;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import org.bouncycastle.jce.provider.BouncyCastleProvider;


public class SampleRSACrypto {
    static final byte[] plainText = "Mesaj de test. Codare folosind algoritmul RSA".getBytes();

	public static void main(String argv[]) {
	
		KeyPairGenerator rsaKeyPairGen;
		KeyPair rsaKeyPair;
		PublicKey rsaPub;
		PrivateKey rsaPriv;
		Cipher cp;
		
		byte[] cipherText = null, newPlainText = null;
		
		
		// Genereaza perechea cheie publica\privata
		try {
		    rsaKeyPairGen = KeyPairGenerator.getInstance("RSA");
		} catch (NoSuchAlgorithmException ex) {
		    System.out.println("Eroare initializare generator chei.");
		    return;
		} 
		
		// initializeaza generatorul de chei
		rsaKeyPairGen.initialize(1024);
		
		//genereaza perechea de chei
		rsaKeyPair = rsaKeyPairGen.generateKeyPair();
		if (rsaKeyPair == null) {
		    System.out.println("Eroare generare pereche cheie publica - privata");
		    return;
		}
		
		// cheia publica RSA
		rsaPub = rsaKeyPair.getPublic();
		
		// cheia privata RSA
		rsaPriv = rsaKeyPair.getPrivate();
		
		// initializeaza obiectul de tip Cipher folosit pentru criptare decriptare 
		try {
		    cp = Cipher.getInstance("RSA");
		} catch (NoSuchAlgorithmException ex) {
		    System.out.println("Algoritmul de criptare RSA nu exista.");
		    return;
		}  catch (NoSuchPaddingException ex) {
		
		    System.out.println("Eroare initializare Cipher.");
			return;
		}
		
		// Criptare
		try {
		    cp.init(Cipher.ENCRYPT_MODE, rsaPub);
		    System.out.println("Text initial: "+new String(plainText));
		    cipherText = cp.doFinal(plainText);
		    System.out.println("Text criptat:"+new String(cipherText));
		    
		} catch (Exception ex) {
		System.out.println("Eroare criptare: " + ex.getMessage());
		return;
		}
		
		// Decriptare
		try {
		    cp.init(Cipher.DECRYPT_MODE, rsaPriv);
		    newPlainText = cp.doFinal(cipherText);
		    System.out.println("Text decriptat:"+new String(newPlainText));
		} catch (Exception ex) {
		    System.out.println("Eroare decriptare: " + ex.getMessage());
		    return;
		}
		
		// Verifica rezultatul
		if (Arrays.equals(plainText, newPlainText)) {
		    System.out.println("Encriptare si decriptare realizata cu succes.");
		} else {
		    System.out.println("Encriptarea si decriptarea nu au fost realizate corect.");
		}
	}
}