/*
 * FileED.java
 */
package lab.scd.encrypt.des;

import java.io.*;

import javax.crypto.SecretKey;

/**
 * Class created by @author Mihai HULEA.
 * 
 * This class is part of the laborator4_2crypto project.
 * 
 */
public class FileED {
    
    DESEncDec ed; 
    
    public FileED(String  filekey){
        byte[] b = getBytesFromFile(filekey);
        ed = new DESEncDec(DESEncDec.loadKey(b));
    }
    
    public FileED(){
        ed = new DESEncDec();
    }
    
    /**
     * Criptare continut fisier sursa si salvare in alt fisier.
     * @param source
     * @param dest
     */
    public void encriptFile(String source, String dest){
        String content = this.readFile(source);
        content = ed.encrypt(content);
        this.writeToFile(dest,content);
    }
    
    public void decriptFile(String source, String dest){
        String content = this.readFile(source);
        content = ed.decrypt(content);
        this.writeToFile(dest,content);
    }

    /**
     * @param string
     */
    private void saveKey(String file) {
        String k = new String(ed.getKeyBytes());
        this.writeToFile(file,k);
    }
        
    String readFile(String filename){
        String content = null;
        try {
            BufferedReader bf = new BufferedReader(new FileReader(filename));
            StringBuffer c = new StringBuffer(100);
            String line = bf.readLine();
            while(line!= null){
              c.append(line);
              line = bf.readLine();
            }
            content = c.toString();           
        } catch (Exception e) {
           
            e.printStackTrace();
            System.exit(0);
        }
        return content;
    }
    
    void writeToFile(String filename, String content){
        
        try {
            FileWriter fw = new FileWriter(new File(filename));
            System.out.println(">"+content);
            fw.write(content);
            fw.close();
        } catch (IOException e) {
            
            e.printStackTrace();
            System.exit(0);
        }
    }
    
    // Returns the contents of the file in a byte array.
    public static byte[] getBytesFromFile(String file){
        
        byte[] bytes=null;
        
        try{
            
        
        InputStream is = new FileInputStream(file);
    
        // Get the size of the file
        long length = file.length();
    
        // You cannot create an array using a long type.
        // It needs to be an int type.
        // Before converting to an int type, check
        // to ensure that file is not larger than Integer.MAX_VALUE.
        if (length > Integer.MAX_VALUE) {
            // File is too large
        }
    
        // Create the byte array to hold the data
        bytes = new byte[(int)length];
    
        // Read in the bytes
        int offset = 0;
        int numRead = 0;
        while (offset < bytes.length
               && (numRead=is.read(bytes, offset, bytes.length-offset)) >= 0) {
            offset += numRead;
        }
    
        // Ensure all the bytes have been read in
        if (offset < bytes.length) {
            throw new IOException("Could not completely read file "+file);
        }
    
        // Close the input stream and return bytes
        is.close();
        }catch(Exception e){
            
            e.printStackTrace();
            System.exit(0);
        }
        return bytes;
    }
    
    public static void main(String[] args) {
        
        /**
         * Foloseste constructorul implicit. Va fi generata automat o cheie.
         */
        FileED ed = new FileED();
        
        System.out.println("Genereaza fisier text pentru teste.");
        ed.writeToFile("test.txt","Continut fisier.Fisier de test.");
        
        System.out.println("Criptare fisier text");
        ed.encriptFile("test.txt","test.enc");
        
        System.out.println("Decriptare fisie");
        ed.decriptFile("test.enc","test.dec");
        
        System.out.println("Salveaza cheia");
        ed.saveKey("test.key");
        
        //-----
        
        /**
         * Foloseste o cheie existenta pentru decriptarea unui fisier
         */
        FileED ed2 = new FileED("test.key");
        
        ed2.decriptFile("test.enc","test.dec2");
        
        
    }

   
}
