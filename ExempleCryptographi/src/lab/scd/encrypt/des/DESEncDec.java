/*
 * DESEncDec.java
 */
package lab.scd.encrypt.des;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.*;
import javax.crypto.spec.SecretKeySpec;

/**
 * Class created by @author Mihai HULEA.
 * 
 * This class is part of the laborator4_2crypto project.
 * 
 * Aceasta clasa implementeaza mecanismele pentru criptarea si decriptarea de
 * string-uri folosind algoritmul DES cu cheie simetricai. 
 */
public class DESEncDec {
    Cipher ecipher;
    Cipher dcipher;
    SecretKey key; 
    
    /**
     * In cadrul construictorului sunt initializate componentele necesare pentru 
     * criptarea decriptarea datelor. In cadrul constructorului se genereaza cheia 
     * folosita pentru criptare\decriptare.
     *  Este folosit aloritmul DES cu cheie simetrica.
     *
     */
    DESEncDec(){
        try {
            
            //genereaza cheia ce va fi folosita in cadrul procesului de criptare decriptare
            key = DESEncDec.generateKey();
            
            //initializeaza obiectul folosy pentru criptarea datelor
            ecipher = Cipher.getInstance("DES");
            ecipher.init(Cipher.ENCRYPT_MODE, key);
            
            //intilizeaza obiectul folosit pentru decriptarea datelor
            dcipher = Cipher.getInstance("DES");
            dcipher.init(Cipher.DECRYPT_MODE, key);
            
        } catch (javax.crypto.NoSuchPaddingException e) {
        } catch (java.security.NoSuchAlgorithmException e) {
        } catch (java.security.InvalidKeyException e) {
        }    
    }
    
 /**
  * In cadrul construictorului sunt initializate componentele necesare pentru
  * criptarea decriptarea datelor.	
  * @param key cheia folosta pentru criptare decriptare
  */   DESEncDec(SecretKey key) {
        try {
            this.key = key;
            System.out.println("Algorithm used to generate key:"+key.getAlgorithm()+".");
            ecipher = Cipher.getInstance("DES");
            dcipher = Cipher.getInstance("DES");
            ecipher.init(Cipher.ENCRYPT_MODE, key);
            dcipher.init(Cipher.DECRYPT_MODE, key);

        } catch (javax.crypto.NoSuchPaddingException e) {
        	e.printStackTrace();
        } catch (java.security.NoSuchAlgorithmException e) {
        	e.printStackTrace();
        } catch (java.security.InvalidKeyException e) {
        	e.printStackTrace();
        }
    }
    
    
/**
 * Metoda realizeaza criptarea unui sir.
 * @param str sirul ce trebuie criptat
 * @return sirul criptat
 */
    public String encrypt(String str) {
        try {
            // Encode the string into bytes using utf-8
            byte[] utf8 = str.getBytes("UTF8");

            // Encrypt
            byte[] enc = ecipher.doFinal(utf8);

            // Encode bytes to base64 to get a string
            return new sun.misc.BASE64Encoder().encode(enc);
        } catch (javax.crypto.BadPaddingException e) {
        } catch (IllegalBlockSizeException e) {
        } catch (UnsupportedEncodingException e) {
        } catch (java.io.IOException e) {
        }
        return null;
    }

    /**
     * Metoda decripteaza un sir
     * @param str sirul ce trebuie decriptat
     * @return sirul decriptat
     */
    public String decrypt(String str) {
        try {
            // Decode base64 to get bytes
            byte[] dec = new sun.misc.BASE64Decoder().decodeBuffer(str);

            // Decrypt
            byte[] utf8 = dcipher.doFinal(dec);

            // Decode using utf-8
            return new String(utf8, "UTF8");
        } catch (javax.crypto.BadPaddingException e) {
        } catch (IllegalBlockSizeException e) {
        } catch (UnsupportedEncodingException e) {
        } catch (java.io.IOException e) {
        }
        return null;
    }
    
    /**
     * Returneaza cheia folosta pentru criptare\decriptare ca un sir de bytes
     * @return
     */
    public byte[] getKeyBytes(){
        return key.getEncoded();
    }
    
    /**
     * Genereaza o cheie.
     * @return
     */
    public static SecretKey generateKey(){
        SecretKey key=null;
        try {

            key = KeyGenerator.getInstance("DES").generateKey();
            System.out.println("Key generated with algorithm:"+key.getAlgorithm());
        } catch (NoSuchAlgorithmException e) {
            
            e.printStackTrace();
            System.exit(0);
        }
        return key;
    }
    
    /**
     * Construieste un obiect de tip cheie be baza unui sir de bytes. In cadrul 
     * acestei aplicatii aceasta metoda este folosita pentru incarcarea unei chei 
     * dintr-un fisier. Cheia este citita din fisier ca un sir de bytes dupa care 
     * este apelata aceasta metoda pentru a genera obiectul de tip SecretKey.
     * @param keyBytes
     * @return
     */
    public static SecretKey loadKey(byte[] keyBytes){
        SecretKey key = new SecretKeySpec(keyBytes, "DES");
        return key;
    }
}