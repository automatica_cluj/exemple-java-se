/*
 * SignatureUtil.java
 */
package lab.scd.encrypt.signature;

import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.io.*;

/**
 * 
 * This class is part of the laborat	or4_2crypto project.
 * 
 * Clasa implementeaza functiile pentru generarea si verificarea de semnaturi 
 * digitale folosind algoritmul DES.
 * 
 * 
 */
public class SignatureUtil {
    
    /**
     * Metoda genereaza semnatura digitala pentru sir de bytes. 
     * @param textForSign sirul de bytes pentru care va fi generata semnatura
     * @param signFile fisierul in care va fi salvata semnatura
     * @param keyFile fisiserul in care va fi salvata cheia publica folosita pentru generarea semnaturii
     * @throws NoSuchAlgorithmException
     * @throws NoSuchProviderException
     * @throws InvalidKeyException
     * @throws SignatureException
     * @throws IOException
     */
    public static void generateSignature(byte[] textForSign, String signFile, String keyFile) throws NoSuchAlgorithmException, NoSuchProviderException, InvalidKeyException, SignatureException, IOException
    {
        /* Genreaza o pereche de chie: publica\privata */

        KeyPairGenerator keyGen = KeyPairGenerator.getInstance("DSA", "SUN");
        SecureRandom random = SecureRandom.getInstance("SHA1PRNG", "SUN");

        keyGen.initialize(1024, random);

        KeyPair pair = keyGen.generateKeyPair();
        PrivateKey priv = pair.getPrivate();
        PublicKey pub = pair.getPublic();


        /* Initializeaza obiectul de tip Signature ce fa fi utilizat pentru generarea semnaturii */

        Signature dsa = Signature.getInstance("SHA1withDSA", "SUN"); 

        dsa.initSign(priv);

        /* Initializeaza obiectul de tip Signature cu textul ce va fi semnat */

        
        dsa.update(textForSign);
        
        /* Genereaza semnatura */

        byte[] realSig = dsa.sign();

    
        /* Salveaza semnatura intr-un fisiser */
        FileOutputStream sigfos = new FileOutputStream(signFile);
        sigfos.write(realSig);

        sigfos.close();


        /* Salveaza cheia publica folosta pentru generarea semnaturii */
        byte[] key = pub.getEncoded();
        FileOutputStream keyfos = new FileOutputStream(keyFile);
        keyfos.write(key);

        keyfos.close();
    }
    
   /**
    * Verifica semnatura digitala pentru un sir de bytes
    * @param keyFile fisiserul in care se gaseste cheia publica
    * @param signature fisiserul in care se gaseste semnatura 
    * @param data sirul de bytes ce va fi verificat 
    * @return true daca sirul de bytes data nu este alterat si corespunde semnaturii digitale
    * @throws IOException
    * @throws NoSuchAlgorithmException
    * @throws NoSuchProviderException
    * @throws InvalidKeySpecException
    * @throws InvalidKeyException
    * @throws SignatureException
    */
    public static boolean verifySignature(String keyFile, byte[] signature, byte[] data) throws IOException, NoSuchAlgorithmException, NoSuchProviderException, InvalidKeySpecException, InvalidKeyException, SignatureException{
        /* importa cheia publica necesara pentru verificarea semnaturii */

        FileInputStream keyfis = new FileInputStream(keyFile);
        byte[] encKey = new byte[keyfis.available()];  
        keyfis.read(encKey);

        keyfis.close();

        X509EncodedKeySpec pubKeySpec = new X509EncodedKeySpec(encKey);

        KeyFactory keyFactory = KeyFactory.getInstance("DSA", "SUN");
        PublicKey pubKey = keyFactory.generatePublic(pubKeySpec);       

        /* Construieste obiectul de tip Signature pentru verificarea semnaturii */
        Signature sig = Signature.getInstance("SHA1withDSA", "SUN");
        sig.initVerify(pubKey);

        /* pregateste datele ce vor fi verificate */
        sig.update(data);

        /* verifica */
        return sig.verify(signature);

    }
    
    public static String readFile(String filename){
        String content = null;
        try {
            BufferedReader bf = new BufferedReader(new FileReader(filename));
            StringBuffer c = new StringBuffer(100);
            String line = bf.readLine();
            while(line!= null){
              c.append(line);
              line = bf.readLine();
            }
            content = c.toString();           
        } catch (Exception e) {
           
            e.printStackTrace();
            System.exit(0);
        }
        return content;
    }
    
    public static void main(String[] args) throws InvalidKeyException, NoSuchAlgorithmException, NoSuchProviderException, SignatureException, IOException, InvalidKeySpecException {
		
		String mesaj ="Message for testing digital signature algorithm.";
		//SignatureUtil.generateSignature(mesaj.getBytes(), "signature.txt", "key.dat");
		
		boolean r = SignatureUtil.verifySignature("key.dat", SignatureUtil.readFile("signature.txt").getBytes(), mesaj.getBytes());
		System.out.println("Messaje is OK = "+r);
	}
}
