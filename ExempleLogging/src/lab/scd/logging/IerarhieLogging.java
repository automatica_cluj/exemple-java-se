/*
 * IerarhieLogging.java
 */
package lab.scd.logging;

import java.util.logging.*;
/**
 * Class created by @author mihai at May 7, 2005.
 * 
 * This class is part of the laborator6_logging project.
 * 
 * In cadrul unui program obiectele de tip logging se afla intr-o relatie
 * de parinte - fiu. Relatia este stabilita pe baza numelui atasat obiectului 
 * de tip Logger, in momentul in care acesta este construit. Obiectul fiu mosteneste
 * caracteristicile obiectului Logger parinte (filtre, handlere, nivel de logare)
 */
public class IerarhieLogging {
    public static void main(String args[]){
        Logger log1 = Logger.getLogger("nivel1");
        Logger log2 = Logger.getLogger("nivel1.nivel2");
        Logger log3 = Logger.getLogger("nivel1.nivel2.nivel3");
        
        //caz 1
        log1.setLevel(Level.SEVERE);
        
        log1.severe("my severe message");
        log1.warning("my warning message");
        log1.info("my info message");

        log2.severe("my severe message");
        log2.warning("my warning message");
        log2.info("my info message");

        log3.severe("my severe message");
        log3.warning("my warning message");
        log3.info("my info message");

        System.err.println("----");
        //caz 2
        log1.setLevel(Level.SEVERE);
        log2.setLevel(Level.INFO);
        
        log1.severe("my severe message");
        log1.warning("my warning message");
        log1.info("my info message");

        log2.severe("my severe message");
        log2.warning("my warning message");
        log2.info("my info message");

        log3.severe("my severe message");
        log3.warning("my warning message");
        log3.info("my info message");
    
    }
}
