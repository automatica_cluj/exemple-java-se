/*
 * LoggFilter.java
 */
package lab.scd.logging;

import java.util.logging.*;
/**
 * Class created by @author mihai 
 * 
 * This class is part of the laborator6_logging project.
 * 
 * Handlerele filtreaza automat mesajele pe baza nivelului de logging setat. Programatorul
 * are posibilitatea de a set un filtru custom in care sa se stabileasca anumite conditii 
 * pentru logg
 */
public class LoggFilter {

    public static void main(String[] args){
        //construieste handler
        ConsoleHandler handler = new ConsoleHandler();
        
        // Seteaza filtrul 
        handler.setFilter(new Filter() {
            public boolean isLoggable(LogRecord record) {
                if(record.getMessage().indexOf("test")!=-1)
                    return false;
                return true;
            }
        });
        
        Logger logger = Logger.getLogger("lab.scd.logging");
        logger.setUseParentHandlers(false);
        
        logger.addHandler(handler);
        logger.info("Mesaj logat");
        logger.info("Mesaj de test");
    }
}
