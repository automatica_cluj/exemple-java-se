package lab.scd.logging;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * TestLogging.java
 */

/**
 * Class created by @author mihai
 * 
 * This class is part of the laborator6_logging project.
 * 
 * Clasa exemplifica folosire mecanismului de logging inclus in sdk 1.5.0
 */
public class TestLogging {
    public static void main(String[] args) {
        // Get a logger; the logger is automatically created if
        // it doesn't already exist
        /**
         * Primul pas este de obtinere a unui obiect de tip Logger.Daca acesta
         * nu exista va fi construit automat si returnat de betoda getLogger()
         */
        Logger logger = Logger.getLogger("lab.scd.logging");
        
        
        /**
         * Mesajele pot fi logate selectand diferite nivele de severitate
         */
        logger.severe("my severe message");
        logger.warning("my warning message");
        logger.info("my info message");
        logger.config("my config message");
        logger.fine("my fine message");
        logger.finer("my finer message");
        logger.finest("my finest message");
        
        /**
         * Este de asemenea posibila logarea exceptiilor
         */
        
        //construieste un obiect de tip exceptie pentru teste 
        Exception ex = new IllegalStateException();
        logger.log(Level.SEVERE, "Uncaught exception", ex);
     }
}