/*
 * LogToFile.java
 */
package lab.scd.logging;

import java.util.logging.*;
import java.io.*;
/**
 * Class created by @author mihai 
 * 
 * This class is part of the laborator6_logging project.
 * 
 * Clasa exemplifica modul in care se poate realiza redirectarea mesajelor de logging 
 * catre un fisier. 
 * OBSERVATIE
 * In configuratia standard hadlerul pentru fisiere va suprascrie fisiserele vechi.
 */
public class LogToFile {
    public static void main(String args[]){
        try {
            /*
             * Construieste un obiect FileHandler responsabil cu redirectarea
             * mesajelor de logging catre un fisier.
             */  
            
            boolean append = true;//specifica daca handelrul va suprascrie sau nu fisierele existente
            FileHandler handler = new FileHandler("my.log",append);
        
            // obtine obiectul de tip logger
            Logger logger = Logger.getLogger("lab.scd.logging");
            
            //seteaza handlerul de mesaje responsabil cu directarea acestora
            logger.addHandler(handler);
            
            logger.info("Mesaj de test.");
            
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
