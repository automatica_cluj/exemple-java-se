/*
 * ThreadPoolTest.java
 */
package lab.scd.fire.tpool;

/**
 * Class created by @author Mihai HULEA at Feb 22, 2005.
 * 
 * This class is part of the labs project.
 * 
 * Exemplifica folosirea threadpool-urilor folosind pachetul java.util.concurrent
 */
import java.util.concurrent.*;
public class ThreadPoolTest {
    public static void main(String[] args) {
        
        //numarul de fire ce vor fi lansate in executie
        int numWorkers = 10;
        
        //numarul de fire ce se pot executa in paralel
        int threadPoolSize = 3;
    
        //constuieste un nou threadpool cu dimensiune data
        ExecutorService tpes =
            Executors.newFixedThreadPool(threadPoolSize);
    
        Worker[] workers = new Worker[numWorkers];
        for (int i = 0; i < numWorkers; i++) {
            workers[i] = new Worker(i);
            tpes.execute(workers[i]);
        }
        
        //executor service nu mai accepta noi taskuri
        tpes.shutdown();
    }
}