/*
 * Buffer.java
 */
package lab.scd.fire.sincronizareProducerConsumer;

import java.util.ArrayList;

/**
 * Class created by @author Mihai HULEA at Feb 22, 2005.
 * 
 * This class is part of the labs project.
 * 
 */
class Buffer
{
	ArrayList content = new ArrayList();	
	synchronized void put(double d)
	{
		content.add(new Double(d));
		notify();
	}
	
	synchronized double get()
	{
		double d=-1;
		try
		{
		if(content.size()==0) wait();
		d = (((Double)content.get(0))).doubleValue();
		content.remove(0);
		}catch(Exception e){e.printStackTrace();}

		return d;
	}
}
