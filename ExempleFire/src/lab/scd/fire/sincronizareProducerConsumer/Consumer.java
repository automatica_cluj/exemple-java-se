/*
 * Consumer.java
 */
package lab.scd.fire.sincronizareProducerConsumer;

/**
 * Class created by @author Mihai HULEA at Feb 22, 2005.
 * 
 * This class is part of the labs project.
 * 
 */


class Consumer extends Thread
{
	private Buffer bf;
	Consumer(Buffer bf)
	{this.bf=bf;}
	
	public void run()
	{
		while (true)
		{
			System.out.println("Am citit : "+this+" >> "+bf.get());
		}
	}
}
