/*
 * FirCititor.java
 */
package lab.scd.fire.pipe_comm;

/**
 * Class created by @author Mihai HULEA at Feb 22, 2005.
 * 
 * This class is part of the labs project.
 * 
 */
import java.io.*;

class FirCititor extends Thread
{
	private PipedInputStream pi;
	FirCititor()
	{
		pi = new PipedInputStream();
	}
	
	public void run()
	{
		try
		{
		while (true)
		{
			if (pi.available()>0)
			{System.out.println("Fir cititor a primit : "+pi.read());}
		}
		}catch(Exception e){}
		
	}
	
	void conect(PipedOutputStream os)throws Exception
	{
		pi.connect(os);
	}
}
