/*
 * FirScriitor.java
 */
package lab.scd.fire.pipe_comm;

import java.io.PipedOutputStream;

/**
 * Class created by @author Mihai HULEA at Feb 22, 2005.
 * 
 * This class is part of the labs project.
 * 
 */
class FirScriitor extends Thread
{
	private PipedOutputStream po;
	FirScriitor(){po = new PipedOutputStream();}
	
	public void run()
	{
		try
		{
		while (true)
		{
		    int d = (int)(10*Math.random());
		    System.out.println("Fir scriitor trimite : "+d);
			po.write(d);
			sleep(400);
		}
		}catch(Exception e){}
	}
	
	PipedOutputStream getPipe(){return po;}
}
