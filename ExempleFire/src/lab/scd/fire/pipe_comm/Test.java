/*
 * Test.java
 */
package lab.scd.fire.pipe_comm;

/**
 * Class created by @author Mihai HULEA at Feb 22, 2005.
 * 
 * This class is part of the labs project.
 * 
 * Programul exeplifica comunicare intre fire de executie utilizand pipe-uri.
 */
public class Test
{

	public static void main(String args[])
	{
		FirCititor fc = new FirCititor();
		FirScriitor fs = new FirScriitor();
		try
		{
			fc.conect(fs.getPipe());
			fc.start();
			fs.start();
		}catch(Exception e)
		{e.printStackTrace();}
	}
}

