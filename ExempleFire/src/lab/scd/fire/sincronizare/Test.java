/*
 * Test.java
 */
package lab.scd.fire.sincronizare;

/**
 * Class created by @author Mihai HULEA at Feb 23, 2005.
 * 
 * This class is part of the labs project.
 * 
 * Studiati rolulul blocurilor sincronizate si modul de functionare a acestora.
 */
public class Test {

    public static void main(String[] args) {
        
        Punct p = new Punct();
        FirSet fs1 = new FirSet(p);
        FirGet fg1 = new FirGet(p);
        
        fs1.start();
        fg1.start();
        
    }
}
