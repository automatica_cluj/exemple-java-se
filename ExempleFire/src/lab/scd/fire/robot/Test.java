/*
 * Test.java
 */
package lab.scd.fire.robot;

/**
 * Class created by @author Mihai HULEA at Feb 23, 2005.
 * 
 * This class is part of the labs project.
 * 
 * Aplicatia realizeaza controlul unui robot. Robotul este deplasat in pozitia 
 * corespunzatoare citita de la operator. Finalizati aplicatia. 
 */
public class Test {
    
    public static void main(String[] args){
        UserInterface UI = new UserInterface();

        Robot robo= new Robot();

        MotorController MC1 = new MotorController(

                                     new Plane(0), UI, robo);

        MotorController MC2 = new MotorController(

                                     new Plane(1), UI, robo);

        MotorController MC3 = new MotorController(

                                     new Plane(2), UI, robo);

        MC1.start();
        MC2.start();
        MC3.start();
    }
}
