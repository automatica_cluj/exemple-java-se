/*
 * MotorController.java
 */
package exemple.fire.robot;

/**
 * Class created by @author Mihai HULEA at Feb 23, 2005.
 * 
 * This class is part of the labs project.
 * 
 */

public class MotorController extends Thread {

    public MotorController(Plane dimension,

           UserInterface UI, Robot robo) { // constructor

      super ();

      dim = dimension;

      myInterface = UI;

      myRobot = robo;

    }

    public void run () {

      int position = 0; // initial position

      int setting;

      while(true) {

        // Get new offset and update position.

        setting = myInterface.newSetting(dim);

        position = position + setting;

        myRobot.move(dim, position); // move to position

      }

    }

    private Plane dim;

    private UserInterface myInterface;

    private Robot myRobot;

  }

class Robot {

    // The interface to the Robot itself.

  public void move (Plane dim, int pos) {}

    // Other methods, not significant here.

  }

class UserInterface {

    // Allows the next position of the robot to be

    // obtained from the operator.

    public int newSetting (Plane dim) { return 0; }

  }

class Plane{
    int planeId;
    public Plane(int planeId){
        this.planeId = planeId;
    }
}
