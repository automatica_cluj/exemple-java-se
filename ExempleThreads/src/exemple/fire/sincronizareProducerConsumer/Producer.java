/*
 * Producer.java
 */
package exemple.fire.sincronizareProducerConsumer;

/**
 * Class created by @author Mihai HULEA at Feb 22, 2005.
 * 
 * This class is part of the labs project.
 * 
 */
class Producer implements Runnable
{
	private Buffer bf;
	private Thread thread;
	Producer(Buffer bf)
	{this.bf=bf;
	}

	public void start()
	{
		if (thread==null)
		{
			thread = new Thread(this);
			thread.start();
		}
	}
	public void run()
	{
		while (true)
		{
			System.out.println("Am scris.");
			bf.put(Math.random());
			try
			{Thread.sleep(1000);
			}catch(Exception e){}
		}
	}
}

