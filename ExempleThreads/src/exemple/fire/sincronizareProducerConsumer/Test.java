/*
 * Test.java
 */
package exemple.fire.sincronizareProducerConsumer;

/**
 * Class created by @author Mihai HULEA at Feb 22, 2005.
 * 
 * This class is part of the labs project.
 * 
 * 
 * Aplicatia exemplifica sincronizarea a mai multe fire de executie pentru accesarea
 * unei resurse comune. Este exeplificata utilizarea metodelor wait() si notify() si accesaea
 * resurselor comune utilizand blocuri synchronized.
 */
public class Test {
    public static void main(String[] args){
        Buffer b = new Buffer();
		Producer pro = new Producer(b);
		Consumer c = new Consumer(b);
		Consumer c2 = new Consumer(b);
		pro.start();
		c.start();
		c2.start();
    }
}
