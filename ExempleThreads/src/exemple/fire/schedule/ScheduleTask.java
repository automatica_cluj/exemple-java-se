/*
 * ScheduleTask.java
 */
package exemple.fire.schedule;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Class created by @author Mihai HULEA at Feb 22, 2005.
 * 
 * This class is part of the labs project.
 * 
 * Programul exeplicfica utilizarea clasei Timer. Aceasat clasa permite planificarea
 * taskurilor pentru executie. Taskurile pot fi periodice. Pentru a putea fi planificat 
 * pentru executie un task trebuie sa fie construit avand la baza clasa TimerTask. 
 * 
 * 1. Testati modul de functionare al programului
 * 
 */
public class ScheduleTask {
  
    Timer timer = new Timer();

    public ScheduleTask() {
        
        timer.schedule(new RemindTask(),
                       0,        //initial delay
                       1*1000);  //subsequent rate
    
        timer.schedule( new SingleExecution(),2000);
    }

    /**
     * Task ce se va executa periodic.
     * Class created by @author Mihai HULEA at Feb 22, 2005.
     * 
     * This class is part of the labs project.
     *
     */
    class RemindTask extends TimerTask {
        int numWarningBeeps = 3;
        public void run() {
            if (numWarningBeeps > 0) {
               
                System.out.println("Beep!");
                numWarningBeeps--;
            } else {
               
                System.out.println("Time's up!");
                
                System.exit(0);   
            }
        
    }
    }
    
    /**
     * Task ce se va executa o singura data.
     * Class created by @author Mihai HULEA at Feb 22, 2005.
     * 
     * This class is part of the labs project.
     *
     */
    class SingleExecution extends TimerTask{
        public void run(){
            System.out.println("Task executed.");
        }
    }
    
    public static void main(String[] args){
        new ScheduleTask();
    }
}