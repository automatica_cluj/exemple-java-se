/*
 * TestModificaTabel.java
 */
package lab.scd.db.jdbc10;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

import lab.scd.db.util.DBConfig;

/**
 * Class created by @author Mihai HULEA at Mar 10, 2005.
 *
 * This class is part of the laborator4_db project.
 *
 * Pentru realizarea de modificari asupra bazei de date se foloseste metoda
 * executeUpdate() din cadrul clasei Statement.
 *
 */
public class TestModificaTabel {

    public static void main(String[] args) {
        try {

            Class.forName("org.apache.derby.jdbc.ClientDriver");

            //conectare la baza de date
            Connection conn = DriverManager.getConnection("jdbc:derby://" + DBConfig.HOST + "/" + DBConfig.DATABASE, DBConfig.USER, DBConfig.PWD);
            System.out.println("Conexiune la baza de date realizata.");

            //contruieste un obiect de tip statement
            Statement stat = conn.createStatement();

            String updateString = "UPDATE STOC "
                    + "SET PROD = 'prod modificat'"
                    + "WHERE PROD LIKE 'produs 1'";

            //modifica inregistrarile din tabel
            stat.executeUpdate(updateString);

            //afiseaza datele din tabel modificate
            ResultSet rs = stat.executeQuery("SELECT * FROM STOC");

            while (rs.next()) {
                String pname = rs.getString("PROD");
                int ppret = rs.getInt("PRET");
                System.out.println("Produs:" + pname + " Pret:" + ppret);
            }//.while

        } catch (Exception e) {
            e.printStackTrace();
        }

    }//.main
}
