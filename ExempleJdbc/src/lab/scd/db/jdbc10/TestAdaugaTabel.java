/*
 * TestAdaugaTabel.java
 */
package lab.scd.db.jdbc10;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

import lab.scd.db.util.DBConfig;

/**
 * Class created by @author Mihai HULEA at Mar 10, 2005.
 *
 * This class is part of the laborator4_db project.
 *
 */
public class TestAdaugaTabel {

    public static void main(String[] args) {
        try {
            Class.forName("org.apache.derby.jdbc.ClientDriver");

            //conectare la baza de date
            Connection conn = DriverManager.getConnection("jdbc:derby://" + DBConfig.HOST + "/" + DBConfig.DATABASE, DBConfig.USER, DBConfig.PWD);
            System.out.println("Conexiune la baza de date realizata.");

            /**
             * Pentru a efectua iperatii asupra bazei de date este nevoie de un
             * obiect de tip Statement.
             */
            Statement stat = conn.createStatement();

            //aduaga tabelul stoc cu doua campuri (prod si pret)
            stat.executeUpdate("CREATE TABLE STOC (PROD VARCHAR(32), PRET INTEGER)");
            System.out.println("Tabel creat.");

            //adauga doua produse in tabel
            stat.executeUpdate("INSERT INTO STOC VALUES ('produs 1' , 2500)");
            stat.executeUpdate("INSERT INTO STOC VALUES ('produs 2' , 7900)");

            System.out.println("Date inserate in tabel.");

            conn.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
