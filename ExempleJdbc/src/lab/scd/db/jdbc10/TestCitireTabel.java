/*
 * TestCitireTabel.java
 */
package lab.scd.db.jdbc10;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

import lab.scd.db.util.DBConfig;

/**
 * Class created by @author Mihai HULEA at Mar 10, 2005.
 *
 * This class is part of the laborator4_db project.
 *
 * Studiati metodele din clasa ResultSet si operatiile pe care le puteti realiza
 * cu acestea.
 *
 */
public class TestCitireTabel {

    public static void main(String[] args) {
        try {
            Class.forName("org.apache.derby.jdbc.ClientDriver");

            //conectare la baza de date
            Connection conn = DriverManager.getConnection("jdbc:derby://" + DBConfig.HOST + "/" + DBConfig.DATABASE, DBConfig.USER, DBConfig.PWD);

            Statement s = conn.createStatement();

            /**
             * Executa interogarea asupra bazei de date. Metoda executeQuery
             * returneaza un obect de tip ResultSet. Acest obiect contine
             * rezultatul interogarii. Clasa ResultSet contine metode ce permit
             * deplasare si afisare continutului fiecarei linii din obiectul de
             * tip ResultSet.
             */
            ResultSet rs = s.executeQuery("SELECT * FROM STOC");

            /**
             * Cu ajutorul metodei next se parcurg liniile rezultate in urma
             * realizarii interogarii. Meotda deplaseaza cursorul pe linia
             * urmatoare si returneaza true daca nu s-a ajuns inca la sfarsitul
             * inregstrarilor. Daca nu mai exista nici o inregistrare si metoda
             * next nu mai poate realiza deplasare pe urmatoarea linie aceasta
             * va returna false.
             */
            while (rs.next()) {

                String pname = rs.getString("PROD");
                int ppret = rs.getInt("PRET");
                System.out.println("Produs:" + pname + " Pret:" + ppret);
            }

            conn.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
