package lab.scd.db.jdbc10;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import lab.scd.db.util.DBConfig;

/*
 * TestTranzactii.java
 */
/**
 * Class created by @author Mihai HULEA at Mar 10, 2005.
 *
 * This class is part of the laborator4_db project.
 *
 */
public class TestTranzactii {

    Connection conn;
    Statement stat;

    public TestTranzactii() throws Exception {
        //incarcare driver petru baza de date
        Class.forName("org.apache.derby.jdbc.ClientDriver");

        //conectare la baza de date
        conn = DriverManager.getConnection("jdbc:derby://" + DBConfig.HOST + "/" + DBConfig.DATABASE, DBConfig.USER, DBConfig.PWD);
        System.out.println("Conexiune la baza de date realizata.");

        stat = conn.createStatement();
    }

    public void testTranzactie(boolean simulateError) {
        try {
            //start tranzactie
            conn.setAutoCommit(false);

            stat.executeUpdate("INSERT INTO STOC VALUES ('produs A' , 2500)");

            //simuleaza aparitia unei probleme
            if (simulateError == true) {
                throw new SQLException("Eroare update.");
            }

            stat.executeUpdate("INSERT INTO STOC VALUES ('produs B' , 7900)");

            //sfarsit tranzactie
            conn.commit();

        } catch (SQLException ex) {

            ex.printStackTrace();
            try {
                conn.rollback();
            } catch (SQLException e) {
                e.printStackTrace();
            }

        }

    }//.

    public void afiseazaTabel() throws SQLException {
        ResultSet rs = stat.executeQuery("SELECT * FROM STOC");

        while (rs.next()) {
            String pname = rs.getString("PROD");
            int ppret = rs.getInt("PRET");
            System.out.println("Produs:" + pname + " Pret:" + ppret);
        }

    }

    public static void main(String[] args) {
        try {

            TestTranzactii t = new TestTranzactii();

            t.testTranzactie(true);

            t.afiseazaTabel();
            
            t.testTranzactie(false);
            
            t.afiseazaTabel();
            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
