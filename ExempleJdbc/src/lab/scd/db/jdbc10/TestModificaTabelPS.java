/*
 * TestModificaTabelPS.java
 */
package lab.scd.db.jdbc10;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import lab.scd.db.util.DBConfig;

/**
 * Class created by @author Mihai HULEA at Mar 10, 2005.
 *
 * This class is part of the laborator4_db project.
 *
 * Ilustreaza modificarea inregistrarilor unui tabel utilizand clasa
 * PreparedStatement. Desi aceasta metoda de realizarea a update-ului asupra
 * unui tabel presupune un numar mai mare de instructiuni decat metoda
 * prezentata in aplicatia TestModificaTabel, aceasta poate fi de ajutor in
 * situatiile in care se relizeaza mai multe modificari asupra tabelelor,
 * folosindu-se bucle for sau while.
 *
 * 1.Modificati aplicatia astfel incat numele produsului ce se doreste a fi
 * modificat si valoare noului nume sa fie citite de la tastatura.
 */
public class TestModificaTabelPS {

    public static void main(String[] args) {
        try {

            Class.forName("org.apache.derby.jdbc.ClientDriver");

            //conectare la baza de date
            Connection conn = DriverManager.getConnection("jdbc:derby://" + DBConfig.HOST + "/" + DBConfig.DATABASE, DBConfig.USER, DBConfig.PWD);

            System.out.println("Conexiune la baza de date realizata.");

            //contruieste un obiect de tip prepared statement
            String updateString = "UPDATE STOC "
                    + "SET PROD = ?"
                    + "WHERE PROD LIKE ?";
            PreparedStatement stat = conn.prepareStatement(updateString);

            stat.setString(1, "produs modifcat PS");
            stat.setString(2, "produs 2");

            stat.executeUpdate();

            //afiseaza datele din tabel modificate
            Statement s = conn.createStatement();
            ResultSet rs = s.executeQuery("SELECT * FROM STOC");

            while (rs.next()) {
                String pname = rs.getString("PROD");
                int ppret = rs.getInt("PRET");
                System.out.println("Produs:" + pname + " Pret:" + ppret);
            }//.while

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
