/*
 * TestConectare.java
 */
package lab.scd.db.jdbc10;

import java.sql.Connection;
import java.sql.DriverManager;
import lab.scd.db.util.*;
//import com.mysql.jdbc.*;
/**
 * 
 * This class is part of the laborator4_db project.
 * 
 * Exemplifica pasii care trebuiesc realizati pentru a realiza o conexiune la o baza de date.
 * 
 * In exemplul de fata se realizeaza o conexiune la o baza de date MYSQL. Pentru functionarea
 * corecta a programului serverul MYSQL trebuie sa fie startat pe calculatorul pe care
 * se verifica programul si trebuie sa existe baza de date testdb.
 */
public class TestConectare {

    public static void main(String[] args) {
            try {
                //incarcare driver petru baza de date
                Class.forName("org.apache.derby.jdbc.ClientDriver");
                
                //conectare la baza de date
                Connection conn = DriverManager.getConnection("jdbc:derby://"+DBConfig.HOST+"/"+DBConfig.DATABASE,DBConfig.USER,DBConfig.PWD);
                System.out.println("Conexiune la baza de date realizata.");
                
                //inchide cnexiune la baza de date
                conn.close();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
   }
}
