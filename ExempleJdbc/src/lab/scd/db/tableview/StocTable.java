/*
 * StocTable.java
 */
package lab.scd.db.tableview;

import java.sql.*;
import java.util.ArrayList;

import javax.swing.table.AbstractTableModel;

import lab.scd.db.util.DBConfig;

/**
 * Class created by @author Mihai HULEA at Mar 10, 2005.
 * 
 * This class is part of the laborator4_db project.
 * 
 */
public class StocTable extends AbstractTableModel{
    
    Connection conn;
    Statement s;
    ResultSet rs;

    ArrayList columnNames = new ArrayList();
    ArrayList data = new ArrayList();    
        
    StocTable()throws Exception{       
            //incarcare driver petru baza de date
            Class.forName("com.mysql.jdbc.Driver");
            
            //conectare la baza de date
            Connection conn = DriverManager.getConnection("jdbc:mysql://"+DBConfig.HOST+"/"+DBConfig.DATABASE+"?user="+DBConfig.USER+"&password="+DBConfig.PWD);
            
            System.out.println("Conexiune la baza de date realizata.");
                                         
            s = conn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
            
            updateCursor();
            
            getTableMetadata();
    }
    
    private void updateCursor() throws SQLException{
        rs = s.executeQuery("SELECT * FROM STOC");
    }
    
    private void getTableMetadata()throws SQLException{
        ResultSetMetaData rsm = rs.getMetaData();
        int col = rsm.getColumnCount();
        for(int i=1; i<=col; i++) {
            columnNames.add(rsm.getColumnName(i));
        }
 
        // Read the data
        while(rs.next()) {
            ArrayList row = new ArrayList();
            data.add(row);
            for(int j=1; j<=col; j++) {
                
                Object o = rs.getObject(j);
                if(o!=null)
                    row.add(o.toString());
                else
                    row.add("NULL");
            }
         }
    }
    

    public String getColumnName(int c) {
        return (String)columnNames.get(c);
    }
    
    public int getColumnCount() {
        return columnNames.size();
    }
    
    public Object getValueAt(int r,int c) {
        ArrayList row = (ArrayList)data.get(r);
        return (String)row.get(c);
    }
    
    public int getRowCount() {
        return data.size();
    }
   

}
