/*
 * App.java
 */
package lab.scd.db.tableview;

import javax.swing.JFrame;

/**
 * Class created by @author Mihai HULEA at Mar 10, 2005.
 * 
 * This class is part of the laborator4_db project.
 * 
 * Aplicatia exemplifica modul de contruire al unei aplicatii ce permite 
 * parcurgerea inregistrarilor unui tabel si afisarea acestora in cadrul 
 * unei interfete grafice.
 * 
 * Este folosita o baza de date testdb ce contine tabelul stoc cu doua campuri:
 * nume si pret.
 * 
 */
public class App {
     
    
    public static void main(String[] args) throws Exception{
        StocTable stoc = new StocTable();
        JFrame frame = new JFrame();
		frame.getContentPane().add(new TableJPanel(stoc));
		frame.pack();
		frame.setVisible(true);
    }
}
