package lab.scd.db.tableview;

import javax.swing.JPanel;
import javax.swing.JFrame;
import java.awt.Dimension;


/**
* This code was generated using CloudGarden's Jigloo
* SWT/Swing GUI Builder, which is free for non-commercial
* use. If Jigloo is being used commercially (ie, by a corporation,
* company or business for any purpose whatever) then you
* should purchase a license for each developer using Jigloo.
* Please visit www.cloudgarden.com for details.
* Use of Jigloo implies acceptance of these licensing terms.
* *************************************
* A COMMERCIAL LICENSE HAS NOT BEEN PURCHASED
* for this machine, so Jigloo or this code cannot be used legally
* for any corporate or commercial purpose.
* *************************************
*/
import javax.swing.JScrollPane;
import java.awt.GridLayout;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.JTable;
public class TableJPanel extends javax.swing.JPanel {
	private JScrollPane scrollP;
	private JTable tableStoc;
	private StocTable st;
	
	public TableJPanel(StocTable st) {
		super();
		this.st = st;
		initGUI();
	
	}
	
	private void initGUI() {
		try {
			GridLayout thisLayout = new GridLayout(1, 1);
			this.setLayout(thisLayout);
			setPreferredSize(new Dimension(400, 300));
            {
                scrollP = new JScrollPane();
                this.add(scrollP);
                {
                    /*TableModel tableStocModel = new DefaultTableModel(
                        new String[][] { { "One", "Two" }, { "Three", "Four" } },
                        new String[] { "Column 1", "Column 2" });
                    */
                    tableStoc = new JTable();
                    scrollP.setViewportView(tableStoc);
                    tableStoc.setModel(st);
                }
            }
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	* Auto-generated method for setting the popup menu for a component
	*/
    private void setComponentPopupMenu(
        final java.awt.Component parent,
        final javax.swing.JPopupMenu menu) {
        parent.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent e) {
                if (e.isPopupTrigger())
                    menu.show(parent, e.getX(), e.getY());
            }
            public void mouseReleased(java.awt.event.MouseEvent e) {
                if (e.isPopupTrigger())
                    menu.show(parent, e.getX(), e.getY());
            }
        });
    }

}
