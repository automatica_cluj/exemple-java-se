/*
 * ModificaRec.java
 */
package lab.scd.db.jdb20;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

import lab.scd.db.util.DBConfig;

/**
 * 
 * This class is part of the laborator4_db project.
 * 
 * Clasa prezinta metodele introduse in JDBC 2.0 pentru parcuregerea si modificarea 
 * inregistrarilor dintr-un ResultSet
 */
public class ModificaRec {

    public static void main(String[] args) {
        try{
            //incarcare driver petru baza de date
            Class.forName("org.apache.derby.jdbc.ClientDriver");

            //conectare la baza de date
            Connection conn = DriverManager.getConnection("jdbc:derby://" + DBConfig.HOST + "/" + DBConfig.DATABASE, DBConfig.USER, DBConfig.PWD);
            
            /**
             * A se observa cei doi parametri folositi pentru construirea obiectului de
             * tip Statement. Primul parametru specifica faptul ca se doreste posibilitate
             * de deplasare a cursorului inainte si inapoi. Al doilea parametru specifica
             * faptul ca se doreste posibilitate de modificare a inregistrarilor din ResultSet.
             */
            Statement s = conn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
				     ResultSet.CONCUR_UPDATABLE);
            
            //executa interogarea asupra bazei de date 
            ResultSet rs = s.executeQuery("SELECT * FROM STOC");
            
            //modifica o inregistrare
            rs.last();
            rs.updateString("PROD","modificat");
            
            //adauga o inregistrare
            rs.moveToInsertRow();
            rs.updateString("PROD","Alt prdod");
            rs.updateInt("PRET",1900);
            rs.insertRow();
            
            //sterge o inregistrare
            rs.absolute(4);
            rs.deleteRow();
            
        }catch(Exception e){
            e.printStackTrace();
        }
    }
}
