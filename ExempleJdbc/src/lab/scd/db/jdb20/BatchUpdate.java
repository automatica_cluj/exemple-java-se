/*
 * BatchUpdate.java
 */
package lab.scd.db.jdb20;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

import lab.scd.db.util.DBConfig;

/**
 * Class created by @author Mihai HULEA at Mar 13, 2005.
 *
 * This class is part of the laborator4_db project.
 *
 * JDBC 2.0 a introdus posibilitatea de a trimite catre baza de date un set de
 * mai multe updateuri ca un set. Acest procedeu este mult mai eficient decat
 * trimiterea fiecarei comenzi de update separat.
 */
public class BatchUpdate {

    public static void main(String[] args) {
        try {

            //incarcare driver petru baza de date
            Class.forName("org.apache.derby.jdbc.ClientDriver");

            //conectare la baza de date
            Connection conn = DriverManager.getConnection("jdbc:derby://" + DBConfig.HOST + "/" + DBConfig.DATABASE, DBConfig.USER, DBConfig.PWD);
            System.out.println("Conexiune la baza de date realizata.");

            //start tranzactie
            conn.setAutoCommit(false);

            try {

                Statement stmt = conn.createStatement();

                stmt.addBatch("INSERT INTO STOC "
                        + "VALUES('Amaretto1', 9898)");
                stmt.addBatch("INSERT INTO STOC "
                        + "VALUES('Hazelnut1', 900)");
                stmt.addBatch("INSERT INTO STOC "
                        + "VALUES('Amaretto_decaf', 77)");
                stmt.addBatch("INSERT INTO STOC "
                        + "VALUES('Bygtoac1', 69)");

                int[] updateCounts = stmt.executeBatch();
                
                conn.commit();

            } catch (Exception e) {

                e.printStackTrace();
                //rollback transaction in caz de eroare in cadrul uneia dintre operatiile de ipdate 

                conn.rollback();
            }

        } catch (Exception e) {

            e.printStackTrace();
        }
    }
}
