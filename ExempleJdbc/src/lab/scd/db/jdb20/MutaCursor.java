/*
 * MutaCursor.java
 */
package lab.scd.db.jdb20;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

import lab.scd.db.util.DBConfig;

/**
 * Class created by @author Mihai HULEA at Mar 13, 2005.
 *
 * This class is part of the laborator4_db project.
 *
 * Exemplifica metodele de deplasare in cadrul unui ResultSet folosind
 * facilitatile din JDBC 2.0
 *
 */
public class MutaCursor {

    public static void main(String[] args) {
        try {
            //incarcare driver petru baza de date
            Class.forName("org.apache.derby.jdbc.ClientDriver");

            //conectare la baza de date
            Connection conn = DriverManager.getConnection("jdbc:derby://" + DBConfig.HOST + "/" + DBConfig.DATABASE, DBConfig.USER, DBConfig.PWD);
            System.out.println("Conexiune la baza de date realizata.");
            
            Statement s = conn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
                    ResultSet.CONCUR_UPDATABLE);

            //executa interogarea asupra bazei de date 
            ResultSet rs = s.executeQuery("SELECT * FROM STOC");

        //deplasare inainte
            while (rs.next()) {
                String pname = rs.getString("PROD");
                int ppret = rs.getInt("PRET");
                System.out.println("Produs:" + pname + " Pret:" + ppret);
            }

        //deplasare inapoi
            rs.last();
            while (rs.previous()) {
                String pname = rs.getString("PROD");
                int ppret = rs.getInt("PRET");
                System.out.println("* Produs:" + pname + " Pret:" + ppret);
            }

            //pozitionare directa
            rs.absolute(3);
            String pname = rs.getString("PROD");
            int ppret = rs.getInt("PRET");
            System.out.println("** Produs:" + pname + " Pret:" + ppret);

            //pozitionare reliativa fata de pozitia curenta
            rs.relative(1);
            pname = rs.getString("PROD");
            ppret = rs.getInt("PRET");
            System.out.println("*** Produs:" + pname + " Pret:" + ppret);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
