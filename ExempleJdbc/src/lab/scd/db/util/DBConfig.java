/*
 * DBConfig.java
 */
package lab.scd.db.util;

/**
 * Class created by @author Mihai HULEA at Mar 27, 2005.
 * 
 * This class is part of the laborator4_db project.
 * 
 */
public class DBConfig {
    
    public static final String HOST = "localhost"; //"10.128.4.220";
    
    public static final String USER="client";//stud";
    
    public static final String PWD="client";//"some_pas";
    
    public static final String DATABASE="testdb";

}
