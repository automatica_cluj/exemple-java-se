package lab.scd.db.movecursor;

import javax.swing.JPanel;
import javax.swing.JFrame;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


/**
* This code was generated using CloudGarden's Jigloo
* SWT/Swing GUI Builder, which is free for non-commercial
* use. If Jigloo is being used commercially (ie, by a corporation,
* company or business for any purpose whatever) then you
* should purchase a license for each developer using Jigloo.
* Please visit www.cloudgarden.com for details.
* Use of Jigloo implies acceptance of these licensing terms.
* *************************************
* A COMMERCIAL LICENSE HAS NOT BEEN PURCHASED
* for this machine, so Jigloo or this code cannot be used legally
* for any corporate or commercial purpose.
* *************************************
*/
import javax.swing.BorderFactory;
import javax.swing.border.BevelBorder;
import com.cloudgarden.layout.AnchorLayout;
import javax.swing.JButton;
import javax.swing.JTextField;
import com.cloudgarden.layout.AnchorConstraint;
import javax.swing.JLabel;
public class StocJPanel extends javax.swing.JPanel implements ActionListener{
	
    StocTable st;
    
    private JLabel lbName;
	private JTextField tfName;
	private JTextField tfPret;
	private JButton bModif;
	private JButton bNew;
	private JButton bNext;
	private JButton bPrev;
	private JLabel lbPret;

	/**
	* Auto-generated main method to display this 
	* JPanel inside a new JFrame.
	*/
	public static void main(String[] args) {
		JFrame frame = new JFrame();
		frame.getContentPane().add(new StocJPanel(null));
		frame.pack();
		frame.show();
	}
	
	public StocJPanel(StocTable st) {
		super();
		this.st = st;
		initGUI();
		//move to first row
		try{
		st.next(this);
		}catch(Exception e){
		    e.printStackTrace();
		}
	}
	
	private void initGUI() {
		try {
			this.setPreferredSize(new java.awt.Dimension(351, 290));
			AnchorLayout thisLayout = new AnchorLayout();
			this.setLayout(thisLayout);
			this.setBorder(BorderFactory.createBevelBorder(BevelBorder.LOWERED, null, null, null, null));
            {
                bModif = new JButton();
                this.add(bModif, new AnchorConstraint(
                    207,
                    87,
                    53,
                    186,
                    AnchorConstraint.ANCHOR_ABS,
                    AnchorConstraint.ANCHOR_ABS,
                    AnchorConstraint.ANCHOR_ABS,
                    AnchorConstraint.ANCHOR_ABS));
                bModif.setText("Modify");
                bModif.setPreferredSize(new java.awt.Dimension(78, 30));
                bModif.addActionListener(this);
            }
            {
                bNew = new JButton();
                this.add(bNew, new AnchorConstraint(
                    207,
                    206,
                    53,
                    70,
                    AnchorConstraint.ANCHOR_ABS,
                    AnchorConstraint.ANCHOR_ABS,
                    AnchorConstraint.ANCHOR_ABS,
                    AnchorConstraint.ANCHOR_ABS));
                bNew.setText("New");
                bNew.setPreferredSize(new java.awt.Dimension(75, 30));
                bNew.addActionListener(this);
            }
            {
                bNext = new JButton();
                this.add(bNext, new AnchorConstraint(
                    160,
                    86,
                    101,
                    182,
                    AnchorConstraint.ANCHOR_ABS,
                    AnchorConstraint.ANCHOR_ABS,
                    AnchorConstraint.ANCHOR_ABS,
                    AnchorConstraint.ANCHOR_ABS));
                bNext.setText("Next");
                bNext.setPreferredSize(new java.awt.Dimension(83, 29));
                bNext.addActionListener(this);
            }
            {
                bPrev = new JButton();
                this.add(bPrev, new AnchorConstraint(
                    161,
                    200,
                    100,
                    65,
                    AnchorConstraint.ANCHOR_ABS,
                    AnchorConstraint.ANCHOR_ABS,
                    AnchorConstraint.ANCHOR_ABS,
                    AnchorConstraint.ANCHOR_ABS));
                bPrev.setText("Previous");
                bPrev.setPreferredSize(new java.awt.Dimension(86, 29));
                bPrev.addActionListener(this);
            }
            {
                tfPret = new JTextField();
                this.add(tfPret, new AnchorConstraint(
                    91,
                    65,
                    169,
                    95,
                    AnchorConstraint.ANCHOR_ABS,
                    AnchorConstraint.ANCHOR_ABS,
                    AnchorConstraint.ANCHOR_ABS,
                    AnchorConstraint.ANCHOR_ABS));
                tfPret.setPreferredSize(new java.awt.Dimension(191, 30));
            }
            {
                lbPret = new JLabel();
                this.add(lbPret, new AnchorConstraint(
                    92,
                    248,
                    168,
                    43,
                    AnchorConstraint.ANCHOR_ABS,
                    AnchorConstraint.ANCHOR_ABS,
                    AnchorConstraint.ANCHOR_ABS,
                    AnchorConstraint.ANCHOR_ABS));
                lbPret.setText("Pret");
                lbPret.setPreferredSize(new java.awt.Dimension(60, 30));
            }
            {
                tfName = new JTextField();
                this.add(tfName, new AnchorConstraint(
                    56,
                    67,
                    204,
                    95,
                    AnchorConstraint.ANCHOR_ABS,
                    AnchorConstraint.ANCHOR_ABS,
                    AnchorConstraint.ANCHOR_ABS,
                    AnchorConstraint.ANCHOR_ABS));
                tfName.setPreferredSize(new java.awt.Dimension(189, 30));
            }
            {
                lbName = new JLabel();
                this.add(lbName, new AnchorConstraint(
                    56,
                    249,
                    204,
                    42,
                    AnchorConstraint.ANCHOR_ABS,
                    AnchorConstraint.ANCHOR_ABS,
                    AnchorConstraint.ANCHOR_ABS,
                    AnchorConstraint.ANCHOR_ABS));
                lbName.setText("Nume");
                lbName.setPreferredSize(new java.awt.Dimension(60, 30));
            }
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	* Auto-generated method for setting the popup menu for a component
	*/
    private void setComponentPopupMenu(
        final java.awt.Component parent,
        final javax.swing.JPopupMenu menu) {
        parent.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent e) {
                if (e.isPopupTrigger())
                    menu.show(parent, e.getX(), e.getY());
            }
            public void mouseReleased(java.awt.event.MouseEvent e) {
                if (e.isPopupTrigger())
                    menu.show(parent, e.getX(), e.getY());
            }
        });
    }

    
    public void actionPerformed(ActionEvent arg0) {
       //verific butonul apasat
        Object o = arg0.getSource();
        try{
        if(o==bNext){
            //buton next apasat 
            st.next(this);
        }

        if(o==bPrev){
            //buton previous apasat
            st.previous(this);
        }
        
        if(o==bNew){
            
            st.newRecord(this);
        }
        
        if(o==bModif){
            st.modifyRecord(this);
        }
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    
    public void setName(String s){
        this.tfName.setText(s);
    }
    
    public void setPret(String s){
        this.tfPret.setText(s);
    }
    
    public String getName(){
        return tfName.getText();
    }
    
    public String getPret(){
        return tfPret.getText();
    }
}
