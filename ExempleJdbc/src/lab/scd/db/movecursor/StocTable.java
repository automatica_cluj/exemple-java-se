/*
 * StocTable.java
 */
package lab.scd.db.movecursor;

import java.sql.*;

import lab.scd.db.util.DBConfig;

/**
 * Class created by @author Mihai HULEA at Mar 10, 2005.
 * 
 * This class is part of the laborator4_db project.
 * 
 */
public class StocTable {
    
    Connection conn;
    Statement s;
    ResultSet rs;
    
    StocTable()throws Exception{       
            //incarcare driver petru baza de date
            Class.forName("com.mysql.jdbc.Driver");
            
            //conectare la baza de date
            Connection conn = DriverManager.getConnection("jdbc:mysql://"+DBConfig.HOST+"/"+DBConfig.DATABASE+"?user="+DBConfig.USER+"&password="+DBConfig.PWD);
            
            System.out.println("Conexiune la baza de date realizata.");
                                         
            s = conn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,ResultSet.CONCUR_UPDATABLE);
            
            updateCursor();
    }
    
    public void updateCursor() throws SQLException{
        rs = s.executeQuery("SELECT * FROM STOC");
    }
    
    public void next(StocJPanel p) throws SQLException{
      if(rs.isLast()==false){  
       rs.next();
       p.setName(rs.getString("PROD"));
       p.setPret(""+rs.getInt("PRET"));
      }
    }
    
    public void previous(StocJPanel p) throws SQLException{
      if(rs.isFirst()==false){
        rs.previous();  
        p.setName(rs.getString("PROD"));
        p.setPret(""+rs.getInt("PRET"));
      }
    }
       
    public void newRecord(StocJPanel p)throws SQLException {
        rs.moveToInsertRow();
              
        rs.updateString("PROD",p.getName());
        rs.updateInt("PRET",Integer.parseInt(p.getPret()));
        
        rs.insertRow();
        

    }

    public void modifyRecord(StocJPanel p)throws SQLException {
        
        rs.updateString("PROD",p.getName());
        rs.updateInt("PRET",Integer.parseInt(p.getPret()));
        rs.updateRow();
    }
}
