/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package designpatterns.mvc.gara;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author evo2
 */
public class ExempluMVC {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Gara cluj = new Gara();
        GaraControler controler = new GaraControler(cluj);
        
        for(int i=0;i<60;i++){
            controler.adaugaTren("TREN "+i);
            try {Thread.sleep(1000);} catch (InterruptedException ex) {}
        }
    }
    
}
