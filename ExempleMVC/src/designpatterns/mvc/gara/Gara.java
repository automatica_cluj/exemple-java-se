/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package designpatterns.mvc.gara;

import java.util.Observable;

/**
 *
 * @author evo2
 */
public class Gara extends Observable{
    
    public static final int EVENIMENT_SEMAFOR_SCHIMBAT = 0;
    public static final int EVENIMENT_SOSESTE_TREN = 1;
    
    private String linia1;
    private boolean semafor;
    
    void setTrenLinia1(String tren){
        linia1 = tren;
        this.setChanged();
        this.notifyObservers(EVENIMENT_SOSESTE_TREN);        
    }
    
    void setSemafor(boolean s){
        semafor = s;
        
        //notitifca view-ul ca s-a schimbat starea semaforului
        this.setChanged();
        this.notifyObservers(EVENIMENT_SEMAFOR_SCHIMBAT); //transmite catre view un parametru suplimentar pentru a informa 
    }
    
    boolean getSemafor(){       
        return semafor;
    }

    String getLinia1() {
       return linia1;
    }
}
