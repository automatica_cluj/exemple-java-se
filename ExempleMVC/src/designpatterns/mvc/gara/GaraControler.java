/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package designpatterns.mvc.gara;

/**
 *
 * @author evo2
 */
public class GaraControler {
    private Gara gara;
    private GaraView garaView;
    
    GaraControler(Gara gara){
        this.gara = gara;
        this.garaView = new GaraView(this);
        //inregistreaza garaView ca si obsever pentru gara. de fiecare data cand starea garii se shcimba view-ul va fi notificat
        this.gara.addObserver(garaView);
        garaView.setVisible(true);
    }
    
    public void schimbaSemafor(){
        gara.setSemafor(!gara.getSemafor());
    }
    
    public boolean adaugaTren(String tren){
        if(gara.getSemafor() == true){
            gara.setTrenLinia1(tren);
            return true;
        }
        else
            return false;
    }
            
}
