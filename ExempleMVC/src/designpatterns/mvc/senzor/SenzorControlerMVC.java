/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package designpatterns.mvc.senzor;

/**
 *
 * @author evo2
 */
public class SenzorControlerMVC {
    Senzor senzor;
    SenzorView view;
    
    SenzorControlerMVC(Senzor s, SenzorView v){
        this.senzor = s;
        this.view  = v;
        this.senzor.addObserver(this.view);        
        view.setVisible(true);
    }
}
