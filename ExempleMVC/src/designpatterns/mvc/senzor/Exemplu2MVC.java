/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package designpatterns.mvc.senzor;

import java.util.Random;

/**
 *
 * @author evo2
 */
public class Exemplu2MVC {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception{
       Senzor s = new Senzor();
       SenzorView v = new SenzorView();
       SenzorControlerMVC mvnc = new SenzorControlerMVC(s, v);
       
       Random r = new Random();
       for(int i = 0;i<100;i++){
          int x = r.nextInt(100);
          s.setValoare(x);
          Thread.sleep(500);
       }
    }
    
}
