/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package designpatterns.mvc.senzor;

import java.util.Observable;

/**
 *
 * @author evo2
 */
public class Senzor extends Observable{
    private int valoare;
    
    
    void setValoare(int v){
        valoare = v;
        this.setChanged();
        this.notifyObservers();
    }
    
    int getValoare(){
        return valoare;
    }
    
}
