/*
 * WriteFile.java
 */
package lab.scd.sm.custom;

/**
 * Class created by @author Mihai HULEA at Apr 3, 2005.
 * 
 * This class is part of the lab3_1_security project.
 * 
 */
import java.io.*;

class MySecurityManager extends SecurityManager
{
	private String pas;
	MySecurityManager(String pas)
	{
		super();
		this.pas=pas;
	}
	
	public void checkWrite(String f)
	{
	}
	
	public void checkWrite(FileDescriptor f)
	{
	}
	
	public void checkRead(String f)
	{
		if(!chkPas()) throw new SecurityException("Citire blocata."); 
	}
	
	public void checkRead(FileDescriptor f)
	{
		if(!chkPas()) throw new SecurityException("Citire blocata.");
	}
	
	private boolean chkPas()
	{
		try
		{
			BufferedReader line = new BufferedReader(new InputStreamReader(System.in));
			if(!(line.readLine()).equals(pas)) return false;
		}catch(Exception e){}
		return true;
	}
}


public class WriteFile
{


	public static void main(String args[])
	{
		try
		{
		
			System.setSecurityManager(new MySecurityManager("sm custom"));
			
	
			FileWriter fw = new FileWriter("file.txt");
			fw.write("ABCD\n");
			fw.write("123\n");
			fw.close();	
			
			BufferedReader fr = new BufferedReader(new FileReader("file.txt"));
			System.out.println(fr.readLine());
			fr.close();
	
		}catch(SecurityException ex)
		{
			ex.printStackTrace();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	
	}
}