
public class CompareStrings {
	public static void main(String[] args) {
		String s1 = "program test";
		String s2 = "program test";
		String s3 = new String("program test");
		String s4 = new String("program test");
		
		//caz 1
		if(s1.equals(s2))
			System.out.println("1. Objects s1 and s2 are equals!");
		else
			System.out.println("2. Objects s1 and s2 are NOT equals!");
		
		//caz 2
		if(s1 == s2)
			System.out.println("3. Objects s1 and s2 are equals!");
		else
			System.out.println("4. Objects s1 and s2 are NOT equals!");
		
		//caz 3
		if(s1 == s3)
			System.out.println("5. Objects s1 and s2 are equals!");
		else
			System.out.println("6. Objects s1 and s2 are NOT equals!");
		
		//caz 4
		if(s3 == s4)
			System.out.println("7. Objects s1 and s2 are equals!");
		else
			System.out.println("8. Objects s1 and s2 are NOT equals!");
		
	}
}

