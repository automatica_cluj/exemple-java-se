package oop.persoana;


public class Test {
    public static void main(String[] args){
        Persoana p1 = new Persoana();      
        Persoana p2 = new Persoana();
        
        p1.afiseaza();
        
        Persoana p3 = new Persoana("adi",250);
        p3.afiseaza();
        
        Persoana p4 = null;
        p4.afiseaza();
        
        
    }
}
