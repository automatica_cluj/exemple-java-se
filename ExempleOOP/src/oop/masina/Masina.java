package oop.masina;


public class Masina {
    String marca;
    Motor m;
    CutieViteza c;
    int cComb;
    int viteza;
    
    Masina(String marca, Motor m, CutieViteza c){
        this.marca = marca;
        this.m = m;
        this.c = c;       
    }
    
    void butonStartStop(){
        m.startStop();
    }
    
    void accelereaz(){     
        if(m.verificStare()==true){
           if(c.treaptaCurenta()==0){
               System.out.println("Masina nu este in viteza!");               
           }else{ 
                viteza = viteza + c.nr - c.treaptaCurenta();
                System.out.println("Accelerez. Viteza="+viteza+"km\\h");
           }
        }
        else
            System.out.println("Motorul nu este pornit!");
    }
    
    void decelereaza(){
        if(m.verificStare()==true){
            if(viteza>0){
                viteza = viteza -1;
                System.out.println("Franez. Viteza="+viteza+"km\\h");
            }                
        }
        else
            System.out.println("Motorul nu este pornit!");
    }
    
    void schimbaVitezaSus(){
        c.sus();
    }
    
    void schimbaVitezaJos(){
        c.jos();
    }
}
