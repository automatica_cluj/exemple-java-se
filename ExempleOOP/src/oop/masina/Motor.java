package oop.masina;


public class Motor {
    String tip;
    int cc;
    boolean stare;
    
    Motor(String tip, int cc){
        this.tip = tip;
        this.cc = cc;
    }
    
    void startStop(){
        if(stare == false){
            stare = true;
            System.out.println("Motor pornit");
        }else
        {
            stare = false;
            System.out.println("Motor oprit");
        }
    }
    
    boolean verificStare(){
        return stare;
    }
    
}
