package oop.masina;


public class CircuitTest {
    public static void main(String[] args){
        Motor m1 = new Motor("diesel", 2000);
        CutieViteza s1 = new CutieViteza(6);
        
        Masina prototip1 = new Masina("Opel", m1, s1);
        
        prototip1.butonStartStop();
        prototip1.schimbaVitezaSus();
        prototip1.accelereaz();
        prototip1.decelereaza();
    }
}
