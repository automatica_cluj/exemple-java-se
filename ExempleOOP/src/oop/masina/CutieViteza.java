package oop.masina;


public class CutieViteza {
    int treapta;
    int nr;

    public CutieViteza(int nr) {
        this.nr = nr;
    }
    
    void sus(){
        if(treapta<nr){
            treapta = treapta +1;
            System.out.println("Incrementeaza treapta "+treapta);
        }
    }
    
    void jos(){
        if(treapta>-1){
            treapta = treapta -1;
            System.out.println("Decrementeaza treapta "+treapta);
        }
    }
    
    int treaptaCurenta(){
        return treapta;
    }
}
