
package oop.catalog;

public class Elev {
    private String nume;
    private int nota;

    public Elev(String nume, int nota) {
        this.nume = nume;
        if(nota<=0||nota>10)
            this.nota = 1;
        else
            this.nota = nota;
    }
    
    void afiseaza(){
        System.out.println("Elev: "+nume+" nota:"+nota);
    }
    
    String getNume(){
        return nume;
    }
    
    int getNota(){
        return nota;
    }
    
    void modifica(int n){
        if(n>0&&n<=10)
            nota = n;
        else
            System.out.println("Valoare nota invalida!");
    }
    
    public boolean equals(Object x){
        Elev e = (Elev)x;
        return e.nume.equals(nume);
    }
    
    public static void main(String[] args){
        Elev e1 = new Elev("Alin",9);
        Elev e2 = new Elev("Adi",6);
        
        e1.afiseaza();
        e2.afiseaza();
        e1.modifica(21);
        e2.modifica(4);
        e1.afiseaza();
        e2.afiseaza();
    }
}
