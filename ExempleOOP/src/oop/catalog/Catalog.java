
package oop.catalog;
import java.util.*;

public class Catalog {
    
    ArrayList<Elev> lista = new ArrayList<Elev>();

    void adauga(Elev e){
        lista.add(e);
    }
    
    void afisare(){
        for(Elev x:lista){
            x.afiseaza();
        }
    }
    
    double calculMedia(){
        double m = 0;
        for (Elev x : lista) {
            //m+=x.getNota();
            m = m + x.getNota();
        }
        return m/lista.size();
    }
    
    void modifica(String nume, int nota){
        for(Elev e:lista){
            if(e.getNume().equals(nume))
                e.modifica(nota);
        }
    }
  
    
}