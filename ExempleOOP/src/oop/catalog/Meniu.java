package oop.catalog;
import java.io.*;

public class Meniu {
    Catalog catalog = new Catalog();
    
    
    void afiseaza(){
        System.out.println("Optiuni");
        System.out.println("1 - Adauaga elev");
        System.out.println("2 - Afiseaza lista elevi");
        System.out.println("3 - Caluleaza media");
        System.out.println("4 - Iesire");
        System.out.println("--------------------------");
    }
    
    void citesteOptiunea() throws Exception{
        BufferedReader consola = new BufferedReader(new InputStreamReader(System.in));
        boolean activ = true;
        do{
            afiseaza();
            System.out.println("Introduceti optiunea [Apoi Enter]");
            String o = consola.readLine();
            if(o.equals("1"))
            {
                System.out.println("Introduceti numele:");
                String n = consola.readLine();
                System.out.println("Introduceti nota:");
                String s = consola.readLine();
                int nota = Integer.parseInt(s);
                Elev e = new Elev(n,nota);
               
                catalog.adauga(e);
            }   
            if(o.equals("2")){
                catalog.afisare();
            }
            if(o.equals("3")){
                double media = catalog.calculMedia();
                System.out.println("Media din catalog este:"+media);
            }
            if(o.equals("4"))
                activ = false;
        }while(activ);
    }
    
     public static void main(String[] args)throws Exception{
         Meniu m = new Meniu();
         m.citesteOptiunea();
     }
}
