package oop.controltemperatura;


public class Test {
    public static void main(String[] args){
        AgregatIncalizre a1 = new AgregatIncalizre();
        AngregatRacire a2 = new AngregatRacire();
        SenzorTemperatura st = new SenzorTemperatura();
        Termostat tr = new Termostat();
        TermostatUI tui = new TermostatUI(tr);
        tui.setVisible(true);
        
        Controler c = new Controler(a1, a2, st, tr, tui);
        c.control();      
        
    }
}
