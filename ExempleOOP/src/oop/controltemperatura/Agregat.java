package oop.controltemperatura;


public abstract class Agregat {
    boolean stare;
    
    void start(){
        stare = true;
        System.out.println("Agregat pornit");
        functionare();
    }
    
    void stop(){
        stare = false;
        System.out.println("Agregat oprit");
    }
    
    abstract void functionare();
}
