package oop.controltemperatura;


public class Controler {
    AgregatIncalizre ai;
    AngregatRacire ar;
    SenzorTemperatura st;
    Termostat tr;
    TermostatUI ui;

    public Controler(AgregatIncalizre ai, AngregatRacire ar, SenzorTemperatura st, Termostat tr, TermostatUI ui) {
        this.ai = ai;
        this.ar = ar;
        this.st = st;
        this.tr = tr;
        this.ui = ui;
    }
    
    void control(){
        while(true){
            try{Thread.sleep(1000);}catch(Exception e){}
            System.out.println("Temeratura="+st.citeste());
            int t = st.citeste();
            ui.afiseazaTemperatura(""+t);
            if(st.citeste()>tr.citesteMax()){
                ai.stop();
                ar.start();
                ui.afiseazaStare("RACIRE");
            }
            else if(st.citeste()<tr.citesteMin()){
                ai.start();
                ar.stop();
                ui.afiseazaStare("(INCALZIRE");
            }
            else{
                ai.stop();
                ar.stop();
                 ui.afiseazaStare("OPRIT");
            }                        
        }//.while
    }
}
