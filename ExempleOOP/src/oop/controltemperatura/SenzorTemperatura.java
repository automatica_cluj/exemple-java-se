package oop.controltemperatura;


import java.util.Random;


public class SenzorTemperatura {
    int v;
    
    int citeste(){
        Random r = new Random();
        v = r.nextInt(40);
        return v;
    }
    
    void scrie(int x){
        v = x;
    }
}
