package introducere.java.controlflow;


public class ForStructure {
	
	/**
	 * Generate an array of random int numbers. 
	 * @return
	 */
	static int[] generateArray(){
		int size = (int)Math.round(Math.random()*15);
		int[] a = new int[size];
		for(int i=0;i<a.length;i++){
			a[i] = (int)Math.round(Math.random()*100);
		}
		return a;
	}
	
	/**
	 * Display the content of an array.
	 * @param a
	 */
	static void displayArray(int[] a){
		for (int i = a.length; --i>=0; ) 
		{ 
			System.out.print("a["+i+"]="+a[i]+" ");
		}	
		System.out.println();
	}
	
	public static void main(String[] args) {
		int a[];	
		a = generateArray();
		displayArray(a);
	}
}
