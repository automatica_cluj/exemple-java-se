package introducere.java.controlflow;

/**
 * Exemplify while and do-while structures. 
 */
public class WhileStructure {

	public static void main(String[] args) {
		 int count = 1;
         while (count < 11) {
              System.out.println("xCount is: " + count);
              count++;
         }
         
         //reset counter and start againg
         count = 1;
         do {
              System.out.println("xxCount is: " + count);
              count++;
         } while (count <= 11);
	}
}
