package introducere.java.controlflow;

/**
 * Exemplify if-else structures
 */
public class IfElse {
	
	static int getMaxValue(int a, int b){
		int tmp;
		if(a>=b)
			return a;
		else
			return b;
	}

	static int getMaxValue2(int a, int b){
		int tmp;
		if(a>=b)
			tmp = a;
		else
			tmp = b;
		return tmp;
	}

	static void testTemperature(int temp){
		if(temp<5)
			System.out.println("Low temperature");
		else if(temp>=5&&temp<24)
			System.out.println("Normal temperature");
		else
			System.out.println("High temperature");
	}
	
	public static void main(String[] args) {
		
		int x = 10, y=34;
		System.err.println("Compare "+x+" with "+y+" .Max value is "+getMaxValue(x,y));
		
		x = x*10;
		y = x-y;
		System.err.println("Compare "+x+" with "+y+" .Max value is "+getMaxValue2(x,y));
		
		
		int temperature = 34;
		testTemperature(temperature);		
	}
}
