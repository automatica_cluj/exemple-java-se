package introducere.java.controlflow;


public class SwitchStructure {
	
	static int countVowel(String word){
		int vcount=0;
		int ccount=0;
		for(int i=0;i<word.length();i++){
			char c = word.charAt(i);
			
			switch(c) {
		        case 'a': vcount++;break;
		        case 'e': vcount++;break;
		        case 'i': vcount++;break;
		        case 'o': vcount++;break;
		        case 'u': vcount++;break;
		        default : ccount++;        
			}
		}		
		return vcount;
	}
	
	
	public static void main(String[] args) {
		String testword="java";
		System.out.println("Count vowel in word:"+testword+" = "+countVowel(testword));
		
		testword="expression";
		System.out.println("Count vowel in word:"+testword+" = "+countVowel(testword));
		
	}
}
