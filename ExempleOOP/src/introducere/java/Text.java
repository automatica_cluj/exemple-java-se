package introducere.java;


public class Text {
     public static void main(String[] args) throws Exception{
         String s1 = "abc";
         String s2 = "123";
         
         System.out.println(s1+s2);
         String s3 = new String("23");
         
         if(s1.equals(s2)){
            System.out.println("Siruri egale!"); 
         }
         else{
             System.out.println("Siruri diferite");
         }
         
         int x = Integer.parseInt(s2);
         int y = Integer.parseInt(s3);
         int z = x + y;
         System.out.println(z);
     }
}
