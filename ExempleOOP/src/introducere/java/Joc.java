package introducere.java;

import java.io.*;
import java.util.*;

public class Joc {
    public static void main(String[] args)throws Exception {
        Random r = new Random();
        BufferedReader t = new BufferedReader(new InputStreamReader(System.in));
      
        int x = r.nextInt(100);     
        int a = -1;
        while(a!=x){
            String s = t.readLine();
            a = Integer.parseInt(s);
            if(a<x)
               System.out.println("Numarul introdus este prea MIC");
            else if(a>x)
               System.out.println("Numarul introdus este prea MARE");
        }

        System.out.println("Ati ghicit!");     
               
    }
}
