
public class Smartphone {
	
	String model;
	int pb;
	int k;
	boolean stare;
	
	Smartphone(){
		
		k = 1;
		model = "Samsung S4";
		System.out.println("Telefon " +model+" construit.");
		pb = 100;
		afiseazaBaterie();
	}
	
	Smartphone(String model){
		
		this.model = model;
		k = 1;
		System.out.println("Telefon " +model+" construit.");
		pb = 100;
		afiseazaBaterie();
	}
	
	Smartphone(String model, int k){
		
		this.model = model;		
		this.k = k;
		System.out.println("Telefon " +model+" construit.");
		pb = 100;
		afiseazaBaterie();
	}
	
	void afiseazaBaterie() {
		System.out.println("Baterie=" + pb + "% model="+model);
	}

	void incarca() {
		if (pb < 100)
			pb++;
		afiseazaBaterie();
		// System.out.println("Incarcat="+pb+"%");
	}
	
	void pornesteOpreste(){
		if(stare==false){
			stare = true;
			System.out.println("Telefon pornit!");
		}
		else{
			stare = false;
			System.out.println("Telefon oprit!");
		}
	}
		
	

	void apeleaza() {
		if(stare==false){
			System.out.println("Telefon oprit! Nu poate fi utilizat!");
			return;
		}
		if (pb <= k) {
			System.out.println("Telefon "+model+" descarcat!");
		} else {
			System.out.println("Telefonul "+model+" este utilizat.");
			pb = pb - k;
			afiseazaBaterie();
		}
	}

	public static void main(String[] args) {
		Smartphone t1 = new Smartphone();
	
		t1.pornesteOpreste();

	
	}
}
