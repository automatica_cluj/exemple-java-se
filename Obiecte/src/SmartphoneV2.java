class Baterie{
	int pb;
	Baterie(){
		pb = 100;
	}
	
	boolean utilizeaza(int x){
		if(pb<=x){
			System.out.println("Baterie descarcata!");
			return false;
		}
		pb = pb - x;
		return true;
	}
	
	void incarca(){
		if(pb<100)
			pb++;
		afiseaza();
	}
	
	void afiseaza(){
		System.out.println("Baterie "+pb+"%");
	}
}

public class SmartphoneV2 {
	static int contor;
	String model;
	int k;
	Baterie b;
	
	SmartphoneV2(String model, int k){
		contor++;
		this.model = model;		
		this.k = k;
		System.out.println("Telefon " +model+" construit.");
		afiseazaBaterie();
	}
	
	void afiseazaBaterie() {
		if(b!=null)
			b.afiseaza();
		else
			System.out.println("Telefonul nu are baterie!");
	}

	void incarca() {
		if(b!=null)
			b.incarca();
		else
			System.out.println("Telefonul nu are baterie!");
	}
	
	void seteazaBaterie(Baterie b){
		if(b!=null){
			System.out.println("Se inlocuieste baterie!");
		}else{
			System.err.println("Se instaleaza baterie!");
		}
		this.b = b;
	}

	void apeleaza() {
		System.out.println("Apelare");
		if(b==null){
			System.out.println("Telefonul nu are baterie.");
			return;
		}
			
		if(b.utilizeaza(k)==false){
			System.out.println("Baterie telefon descarcata.");
		}else{
			System.out.println("Telefonul "+model+" este utilizat.");
		}
	}

	public static void main(String[] args) {
	
		SmartphoneV2 t4 = new SmartphoneV2("Nexus 4",3);
		
		t4.apeleaza();
		
		Baterie b1 = new Baterie();
		
		t4.seteazaBaterie(b1);
		
		t4.apeleaza();
		
		System.out.println("Telefone construite = "+SmartphoneV2.contor);
	}
}
