
public class Senzor {
	static int k;
	int value;
	boolean active;
	
	public static void main(String[] args) {
		Senzor s1 = new Senzor();
		Senzor s2 = new Senzor();
		
		s1.value = 100;
		s2.value = 200;
		s1.k = 50;
		System.out.println("Senzor s1:"+s1.value);
		System.out.println("Senzor s2:"+s2.value);
		
		System.out.println("Senzor s1 k:"+s1.k);
		System.out.println("Senzor s2 k:"+s2.k);
		System.out.println("Senzor s2 k:"+Senzor.k);
				
	}
}
