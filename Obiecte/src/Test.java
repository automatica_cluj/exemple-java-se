
public class Test {

	public static void main(String[] args) {
		Senzor s1 = new Senzor();
		Senzor s2 = new Senzor();
		
		s1.value = 100;
		s2.value = 200;
		
		System.out.println("Senzor s1:"+s1.value);
		System.out.println("Senzor s2:"+s2.value);

	}

}
