public class CoffeTest {
 
            public static void main(String[] args) {
                        CoffeMaker maker1 = new CoffeMaker();     
                        
                        Coffee[] pachet = new Coffee[10];
                        for (int i = 0; i < pachet.length; i++) {
							pachet[i] = maker1.getCofee();
						}
                        
                        for (int i = 0; i < pachet.length; i++) {
							pachet[i].drinkCofee();
						}
                        
            }
}
 
class CoffeMaker{
            CaffeineTank ctank = new CaffeineTank();
            WaterTank wtank = new WaterTank();
            CoffeMaker(){
                        System.out.println("New cofee maker created.");
            }
            Coffee getCofee(){
                        int w = wtank.getIngredient();
                        int c = ctank.getIngredient();
                        return new Coffee(w,c);
            }
}
 
class  CaffeineTank{
            CaffeineTank(){
                        System.out.println("New coffeine tank created.");
            }
            int getIngredient(){
                        return (int)(Math.random()*10);
            }
}
 
class WaterTank{
            WaterTank(){
                        System.out.println("New water tank created.");
            }
            int getIngredient(){
                        return (int)(Math.random()*40);
            }
}
 
class Coffee {
            int water;
            int  caffeine;
            Coffee(int water, int caffeine){
                        this.water = water;this.caffeine= caffeine;
            }
            void drinkCofee(){
                        System.out.println("Drink cofee [water="+water+":coffe="+ caffeine+"]");
            }
}