
public class TestMyException {
            public static int f(int x, int y) throws MyException {
                  if(x==0&&y==0)
                	  throw new MyException();
                  else
                	  return x + y;
            }
            
            public static void g() throws MyException2 {
                 // System.out.println("Exceptie in g()");
                  throw new MyException2("aruncata din g()");
            }
            
            public static void main(String[] args){
            	
            	System.out.println("PAS 1");
            	
            	try {
            	  int x = 10;	
            	  System.out.println("PAS 2");	
                  f(0,0);
                  System.out.println("PAS 3");
                  g();
                  System.out.println("PAS 4");
            	} catch(MyException e) {
            		System.out.println("TRATARE EROARE");
            		e.printStackTrace();
            	} catch(MyException2 e){
            		System.out.println("TRATARE EROARE 2");
            		e.printStackTrace();
            	} finally{
            		System.out.println("PAS 5");
            	}
            	
            	System.out.println("PAS 6");
	            /*try {
	                  g();
	            } catch(MyException e) {e.printStackTrace();}*/
        
            }
      }
 
class MyException extends Exception {
      public MyException() {}
      public MyException(String msg) {
            super(msg);
      }
}

class MyException2 extends Exception {
    public MyException2() {}
    public MyException2(String msg) {
          super(msg);
    }
}
