import java.io.*;

public class Scriere {	
	public static void main(String[] args) {
		
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		
		try{
			PrintWriter f = new PrintWriter(
		              new BufferedWriter(new FileWriter("ispdemo.txt")));
			
			String linie = "";
			linie = in.readLine();
			while(!linie.equals("sfarsit")){
				f.println(linie);
				linie = in.readLine();				
			}			
			f.close();
		}catch(IOException e){
			e.printStackTrace();
		}
	}

}
