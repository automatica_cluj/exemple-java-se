package lab.scd.net.chatsimple;

import java.util.Scanner;
import java.net.*;
import java.io.*;

public class CClient extends Thread {
	BufferedReader in;
	PrintWriter out;
	
	@Override
	public void run() {
		try {
			Socket s = new Socket("localhost",1999);
			
			out = new PrintWriter(new OutputStreamWriter(s.getOutputStream()),true);
			in = new BufferedReader(new InputStreamReader(s.getInputStream()));
			
			while(true){
				String msg = in.readLine();
				System.out.println("rcv:"+msg);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} 
	}
	
	public void console(){
		Scanner s = new Scanner(System.in);
		while(true){
			if(s.hasNextLine()){
				String mesaj = s.nextLine();
				out.println(mesaj);
			}
		}		
	}
	
	public static void main(String[] args) {
		CClient c = new CClient();
		c.start();
		c.console();
	}
}
